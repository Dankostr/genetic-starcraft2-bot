import yaml
from sc2 import Difficulty, Race

from utils.singleton import Singleton


class Config(object, metaclass=Singleton):

    def __init__(self):
        with open("application.yml") as f:
            self._configuration = {k: v for k, v in Config.__flatten_dict(yaml.safe_load(f))}
            highest_point_on_curve = self.get_solution_optimal_length()
            self.factor = 1 / ((highest_point_on_curve - self.get_solution_min_length()) * (highest_point_on_curve -
                                                                                            self.get_solution_max_length()))

    def __getitem__(self, item):
        return self._configuration.get(item)

    @staticmethod
    def __flatten_dict(pyobj, keystring=''):
        if type(pyobj) == dict:
            keystring = keystring + '.' if keystring else keystring
            for k in pyobj:
                yield from Config.__flatten_dict(pyobj[k], keystring + str(k))
        else:
            yield keystring, pyobj

    def get_main_population_size(self):
        """
        Return configured main population size

        :return: configured main population size
        """
        return self._configuration["APP.GENETIC.MAINPOPULATION.SIZE"]

    def get_second_population_size(self):
        """
        Return configured second population size

        :return: configured seoncd population size
        """
        return self._configuration["APP.GENETIC.SECONDPOPULATION.SIZE"]

    def get_solution_min_length(self):
        """
        Return configured minimum solution length

        :return: configured minimum solution length
        """
        return self._configuration["APP.GENETIC.SOLUTION.MINIMUMLENGTH"]

    def get_solution_max_length(self):
        """
        Return configured maximum solution length

        :return: configured maximum solution length
        """
        return self._configuration["APP.GENETIC.SOLUTION.MAXIMUMLENGTH"]

    def get_solution_factor(self):
        """
        Return calculated factor that should be used in calculating scaled fitness value

        :return: configured calculated factor
        """
        return self.factor

    def get_algorithm_mutation_rate(self):
        """
        Return configured mutation rate

        :return:  configured mutation rate
        """
        return 1-self._configuration["APP.GENETIC.ALGORITHM.MUTATIONRATE"]

    def get_algorithm_crossover_on_populations_needed_iterations(self):
        """
        Return configured iterations required for crossover of populations

        :return: configured iterations required for crossover of populations
        """
        return self._configuration["APP.GENETIC.ALGORITHM.CROSSPOPULATION.ITERATIONS"]

    def get_difficulty(self):
        """
        Return configured difficulty for in-game bot

        :return: configured difficulty for in-game bot
        """
        diff = self._configuration["APP.GAME.ENEMY.DIFFICULTY"]
        for difficulty in list(Difficulty):
            if difficulty.name == diff:
                return difficulty

    def get_race(self):
        """
        Return configured race for in-game bot

        :return: configured race for in-game bot
        """
        enemy_race = self._configuration["APP.GAME.ENEMY.RACE"]
        for race in list(Race):
            if race.name == enemy_race:
                return race

    def get_solution_optimal_length(self):
        """
        Return calculated optimal length for a solution

        :return: calculated optimal length for a solution
        """
        return self.get_solution_max_length() - self.get_solution_min_length()

    def use_upgrades(self):
        """
        Return if bot should use upgrades

        :return: should bot use upgrades
        """

        return self._configuration["APP.GENETIC.ALGORITHM.USEUPGRADES"]

    def get_semi_correct_ratio(self):
        """
        Return configured ratio in which generated semi valid solution should be accepted in main population

        :return: configured ratio in which generated semi valid solution should be accepted in main population
        """
        return 1 - self._configuration["APP.GENETIC.ALGORITHM.SEMICORRECTACCEPTANCERATIO"]
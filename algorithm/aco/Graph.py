from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class Graph(object):

    def __init__(self):
        self.pheromones = {}
        self.ratio = 0.1

    def get_neighborhood_of_vector(self, path: list):
        neighborhood = set()
        set_path = set(path)
        for unit in list(ZergUnits.Units):
            if ZergUnits.dependencies[unit] is None or set(ZergUnits.dependencies[unit]).issubset(set_path):
                neighborhood.add(unit)
        for building in list(ZergBuildings.Buildings):
            if ZergBuildings.dependencies[building] is None or set(ZergBuildings.dependencies[building]).issubset(
                    set_path):
                neighborhood.add(building)
        return list(neighborhood)

    def get_pheromone(self, s_vertex, e_vertex, dimension, iteration):
        value = self.pheromones.get((s_vertex, e_vertex, dimension))
        if value is None:
            return 1 / (dimension * iteration)
        return value

    def update_pheromone_per_path(self, path):
        pheromone_value = 1/ len(path)
        s_vertex = path[0]
        dim = 1
        for e_vertex in path[1:]:
            self.add_pheromone(s_vertex, e_vertex, dim, pheromone_value)
            dim += 1
            s_vertex = e_vertex

    def add_pheromone(self, s_vertex, e_vertex, dimension, pher_value):
        value = self.pheromones.get((s_vertex, e_vertex, dimension))
        if value is None:
            self.pheromones[(s_vertex, e_vertex, dimension)] = pher_value
        else:
            self.pheromones[(s_vertex, e_vertex, dimension)] += pher_value

    def vaporise(self):
        for edge in self.pheromones.keys():
            self.pheromones[edge] *= self.ratio

    def pheromone_values_and_sum(self, starting_point, neighbours, dimension, iterations):
        s_point = starting_point
        sum_value = 0
        d = {}
        for e_vertex in neighbours:
            value = self.get_pheromone(s_point, e_vertex, dimension, iterations)
            d[e_vertex] = value
            sum_value += value
        return d, sum_value

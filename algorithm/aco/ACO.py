import random
import time

from algorithm.aco.Graph import Graph
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class ACO(object):

    def __init__(self):
        self.graph = Graph()

    def start(self):
        t = time.time() + 1
        iteration = 1
        while time.time() < t:
            all_ants = []
            for _ in range(200):
                ant_path,zerglings = self.simulate_ant(iteration)
                if zerglings>0:
                    all_ants.append(ant_path)
            for ant in all_ants:
                self.graph.update_pheromone_per_path(ant)
            self.graph.vaporise()
            iteration += 1
        return self.simulate_ant(iteration)

    def simulate_ant(self, iteration):
        ant_path = [ZergBuildings.Buildings.HATCHERY]
        dimension = 1
        zerglings = 0
        while zerglings < 2 and dimension<10:
            vertexes = self.graph.get_neighborhood_of_vector(ant_path)
            pheromones, sum_pheromones = self.graph.pheromone_values_and_sum(ant_path[-1], vertexes, dimension,
                                                                             iteration)
            probability = {key: value / sum_pheromones for key, value in pheromones.items()}
            p = random.random()
            v = 0
            for key in probability.keys():
                v += probability[key]
                if v < p:
                    pass
                else:
                    ant_path.append(key)
                    if key == ZergUnits.Units.ZERGLING:
                        zerglings += 1
                    break
            dimension+=1
        return ant_path,zerglings

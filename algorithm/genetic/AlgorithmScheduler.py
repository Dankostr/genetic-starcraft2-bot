from queue import Queue
from threading import Thread, Event

from algorithm.genetic.population.MainPopulation import MainPopulation
from algorithm.genetic.population.SecondPopulation import SecondPopulation
from config import Config


class AlgorithmScheduler(Thread):

    def __init__(self, stop_event: Event, queue: Queue, start_calculating_event: Event, finish_event: Event):
        """
        Thread object that manages process of genetic algorithm

        :param stop_event: Event to stop algorithm from working
        :param queue: Queue on which solutions should be put
        :param start_calculating_event: Event to resume genetic algorithm
        :param finish_event: Event to close algorithm
        """
        super().__init__()
        self.stop_event = stop_event
        self.main_population = MainPopulation()
        self.second_population = SecondPopulation()
        self.start_calculating_event = start_calculating_event
        self.queue = queue
        self.finish_event = finish_event
        self.crossover_population = Config().get_algorithm_crossover_on_populations_needed_iterations()

    def run(self) -> None:
        """
        Method to start a thread.
        It manages genetic algorithm and populations
        """
        while not self.finish_event.is_set():
            iteration = 0
            while (
                    not self.stop_event.is_set() or not self.main_population.valid_canditate()) and not self.finish_event.is_set():
                self.main_population.manage_population()
                self.second_population.manage_population()
                iteration += 1
                if iteration >= self.crossover_population:
                    iteration = 0
                    self.__crossover_population_breeding()
            self.queue.put(self.main_population.get_best_candidate())

            while not self.start_calculating_event.is_set():
                continue
            self.start_calculating_event.clear()
            self.main_population = MainPopulation()
            self.second_population = SecondPopulation()

    def __crossover_population_breeding(self):
        """
        Method to initialize crossover between populations
        """
        self.main_population.crossover_selection_between_populations(self.second_population)

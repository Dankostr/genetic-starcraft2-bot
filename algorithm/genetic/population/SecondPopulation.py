import random

from algorithm.genetic.Gene import Gene
from algorithm.genetic.population.PopulationInterface import PopulationInterface
from config import Config


class SecondPopulation(PopulationInterface):

    def __init__(self):
        """
        Initialize second population.
        Generates starting population for second population.
        Population size can be configured in configuration file
        """
        super().__init__()
        self.max_length = Config().get_solution_optimal_length()
        self.size = Config().get_second_population_size()
        for i in range(self.size):
            self.genes.append(Gene())

    def selection(self):
        """
        Selection operator for second population.
        It shortens the length of the genes to optimal length.
        """
        self.genes = list(filter(lambda x: len(x.build_order) > 0, self.genes))
        self.genes = random.sample(self.genes, self.size)
        for gene in self.genes:
            gene.shorten_gene_length_to(self.max_length)

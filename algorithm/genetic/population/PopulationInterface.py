import random

from algorithm.genetic.GeneticAlgorithm import GeneticAlgorithm
from config import Config


class PopulationInterface(object):

    def __init__(self):
        """
        Constructor for interface shared by populations
        """
        self.mutation_chance = Config().get_algorithm_mutation_rate()
        self.genes = []

    def selection(self):
        pass

    def size_of_population(self):
        """
        Return size of a population

        :return: Size of a population
        """
        return len(self.genes)

    def manage_population(self):
        """
        Method to perform crossover, mutation and selection operations on population
        """
        self.new_generation()
        self.selection()

    def new_generation(self):
        """
        Generates child genes from current generation of genes
        """
        population_size = self.size_of_population()
        new_genes = []
        for i in range(population_size // 4):
            new_genes.append(GeneticAlgorithm.cross_in_population(self.genes))
        for i in range(population_size):
            if random.random() >= self.mutation_chance:
                new_genes.append(GeneticAlgorithm.mutate(self.genes))
        self.genes.extend(new_genes)

    def crossover_selection_between_populations(self, other):
        """
        Performs crossover operation between populations then selection on current population

        :param other: Other population
        """
        if other.size_of_population() > 0:
            self.__new_generation_by_crossover_between_populations(other)
            self.selection()

    def __new_generation_by_crossover_between_populations(self, other):
        """
        Method to create new generation of genes by crossover operation over 2 populations

        :param other:
        """
        self_population_size = self.size_of_population()
        other_population_size = other.size_of_population()
        new_genes = []
        for i in range(max(self_population_size, other_population_size) // 4):
            new_genes.append(GeneticAlgorithm.cross_between_populations(self.genes, other.genes))
        self.genes.extend(new_genes)
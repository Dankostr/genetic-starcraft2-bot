import random


from algorithm.genetic.Gene import Gene
from algorithm.genetic.fitnessFunction.FitnessResult import FitnessResult
from algorithm.genetic.fitnessFunction.fitnessfunction import FitnessFunction
from algorithm.genetic.population.PopulationInterface import PopulationInterface
from config import Config
from starcraft.GameState import GameState
from starcraft.ZergValues import ZergValues


class MainPopulation(PopulationInterface):

    def __init__(self):
        """
        Constructor for main population
        Initializes main population with starting genes.
        """
        super().__init__()
        self.judge = FitnessFunction()
        self.game_state = GameState()
        self.zerg_values = ZergValues()
        self.population_max_size = Config().get_main_population_size()
        self.semi_correct_result_accept_ratio = Config().get_semi_correct_ratio()
        game_state_snapshot = self.game_state.snapshot()
        zerg_values_snapshot = self.zerg_values.snapshot()
        for i in range(self.population_max_size):
            gene = Gene()
            while not self.judge.fitness(gene, game_state_snapshot, zerg_values_snapshot
                                         ) != FitnessResult.invalid_build_order():
                if random.random() > self.semi_correct_result_accept_ratio:
                    break
                gene = Gene()
            self.genes.append(gene)

    def selection(self):
        """
        Selection operator for Main population.
        Selection operator is based on elitism.
        """
        game_state_snapshot = self.game_state.snapshot()
        zerg_values_snapshot = self.zerg_values.snapshot()
        self.genes.sort(
            key=lambda gene: self.judge.fitness(gene, game_state_snapshot, zerg_values_snapshot),
            reverse=True)
        self.genes = self.genes[:self.population_max_size]

    def get_best_candidate(self):
        """
        Return first gene from sorted list of genes. This gene had best value in last iteration of the algorithm

        :return: Gene
        """
        return self.genes[0]

    def valid_canditate(self):
        """
        Checks if first gene from sorted list is valid gene

        :return: boolean indicating validation of gene
        """
        return self.judge.fitness(self.genes[0],
                                  self.game_state.snapshot(), self.zerg_values.snapshot()
                                  ) != FitnessResult.invalid_build_order()

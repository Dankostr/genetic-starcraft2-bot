from algorithm.genetic.Gene import Gene
from algorithm.genetic.fitnessFunction.FitnessResult import FitnessResult
from algorithm.genetic.fitnessFunction.InvalidBuildOrderException import InvalidBuildOrderException
from algorithm.genetic.fitnessFunction.QueueBuilding import QueueBuilding
from algorithm.genetic.fitnessFunction.VirutalGameState import VirtualGameState
from starcraft.GameStateSnapshot import GameStateSnapshot
from starcraft.ZergValuesSnapshot import ZergValuesSnapshot
from starcraft.wrapper.SC2Helper import SC2Helper
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits
from starcraft.wrapper.ZergUpgrades import ZergUpgrades
from utils.singleton import Singleton


class FitnessFunction(object, metaclass=Singleton):

    def __init__(self):
        self.sc2_helper = SC2Helper()

    def fitness(self, gene: Gene, game_state: GameStateSnapshot, zerg_values: ZergValuesSnapshot) -> FitnessResult:
        """
        Method to asses fitness result of a gene, with current game state and fitness values

        :param gene: Gene to be assessed
        :param game_state: Snapshot of a game state
        :param zerg_values: Snapshot of current fitness values
        :return:
        """
        virtual_state = VirtualGameState(game_state, zerg_values)
        try:
            for item in gene.get_build_order():
                self.simulate_for_item(item, virtual_state)
            while len(virtual_state.queue) != 0:
                self.wait_for_next_item_created_from_queue(virtual_state)
        except InvalidBuildOrderException:
            return FitnessResult.invalid_build_order()
        return FitnessResult(virtual_state.fit_value, len(gene.get_build_order()),
                             virtual_state.timer,
                             virtual_state.crystals, virtual_state.vespene,virtual_state.cps,
                             virtual_state.vps)

    def simulate_for_item(self, item, state: VirtualGameState):
        """
        Method to simulate virtual game state for given item

        :param item: Item to be used in simulation
        :param state: Virtual game state that is used in fitness function
        """
        if self.can_produce_item(item, state):
            if self.have_resources_for_building(item, state):
                self.start_building(item, state)
            elif self.will_economy_allow_for_building(item, state):
                self.simulate_economy_until_item_can_be_build(item, state)
                self.start_building(item, state)
            else:
                self.wait_for_next_item_created_from_queue(state)
                self.simulate_for_item(item, state)
        else:
            self.wait_for_next_item_created_from_queue(state)
            self.simulate_for_item(item, state)

    def wait_for_next_item_created_from_queue(self, state):
        """
        Method to wait for next item that is building.
        It simulates economy to given point of time as well as moves time in virtutal game state

        :param state: object of virtual game state used during assessment of gene
        """
        queue_item = self.pop_next_created_item(state)
        self.simulate_economy_until_time(queue_item.get_end_time(), state)
        self.manage_created_item(queue_item, state)

    def can_produce_item(self, item, state: VirtualGameState):
        """
        Check if item dependecies are met, if item can be created.
        Throws Invalid build order exception if item is and building or upgrade and was already created/researched in game

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: Boolean if item can be produced
        """
        if not self.is_able_to_build( item, state):
            return False
        if self.__check_if_item_can_be_built(item, state):
            raise InvalidBuildOrderException
        if self.cannot_produce_army_unit(item, state):
            return False
        return True

    def __check_if_item_can_be_built(self, item,
                                     state: VirtualGameState):
        """
        Check if given item was already created/researched in game.
        If item is instance of ZergBuildings.Buildings or ZergUpgrades.Upgrades and was created it returns True
        otherwise returns False. Exception to this rule are Extractors and Hatcheries witch should be build many
        times during a game

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: boolean value indicating if item was already built
        """
        if isinstance(item, ZergBuildings.Buildings):
            return self.check_if_building_cannot_be_build(item, state)
        elif isinstance(item, ZergUpgrades.Upgrades):
            return self.__check_if_upgrade_already_research(item, state)
        return False

    def __check_if_upgrade_already_research(self, item: ZergUpgrades.Upgrades, state: VirtualGameState):
        """
        Check if upgrade was already researched

        :param item: ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return:
        """
        if item in state.upgrades:
            return True
        return False

    def check_if_building_cannot_be_build(self, item: ZergBuildings.Buildings, state: VirtualGameState):
        """
        Check if ZergBuildings.Buildings item cannot be build with respects to rules of fitness function

        :param item: ZergBuildsings.Buildings enum
        :param state: Virtual game state of the current Gene
        :return: Boolean if  bulding can be build
        """
        if item == ZergBuildings.Buildings.EXTRACTOR:
            if state.vespene_extractors >= state.townhalls * 2:
                return True
            return False
        if item == ZergBuildings.Buildings.HATCHERY:
            if state.timer / 60 // 3 >= state.townhalls:
                return False
            return True
        if item.type_id in state.buildings or state.is_item_in_queue(item):
            return True
        if item == ZergBuildings.Buildings.LAIR:
            if ZergBuildings.Buildings.LAIR.type_id in state.buildings or ZergBuildings.Buildings.HIVE.type_id in \
                    state.buildings:
                return True
        if item == ZergBuildings.Buildings.SPIRE:
            if ZergBuildings.Buildings.GREATER_SPIRE.type_id in state.buildings:
                return True
        return False

    def cannot_produce_army_unit(self, item, state: VirtualGameState):
        """
        Check if item is army unit, if so , checks if there is enough slots for this unit and necessary units are
        created if needed

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: boolean represeting if unit cannot be produced
        """
        if isinstance(item, ZergUnits.Units):
            if self.sc2_helper.get_unit_army_slots(item) <= state.slots:
                if item == ZergUnits.Units.BANELING:
                    if state.zerglings <= 0:
                        return True
                if item == ZergUnits.Units.BROOD_LORD:
                    if state.corruptor <= 0:
                        return True
                return False
            return True
        return False

    def is_able_to_build(self, item, state: VirtualGameState):
        """
        Method to check whether dependencies for item are constructed

        :param item: item to be built
        :param state: Virtual game state of the current Gene
        :return:
        """
        if state.drones <= 0:
            return False
        dependencies = self.sc2_helper.get_unit_dependencies(item)
        if dependencies is None:
            return True
        dependencies = set(map(lambda x: x.value, dependencies))
        if dependencies.issubset(state.buildings):
            return True
        return False

    def have_resources_for_building(self, item, state: VirtualGameState):
        """
        Check if in current state bot would have enough resources to create/research an item

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: boolean indicating if bot would have enough resources
        """
        crystal_cost, vespene_cost = self.sc2_helper.get_resources_costs(item)
        if crystal_cost <= state.crystals and vespene_cost <= state.vespene:
            return True
        return False

    def will_economy_allow_for_building(self, item, state: VirtualGameState):
        """
        Checks if it would be possible to build/research an item in future given current state of economy

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: boolean  indicating if bot will reach necessary resources
        """
        crystal_cost, vespene_cost = self.sc2_helper.get_resources_costs(item)
        if (crystal_cost > 0 >= state.cps) or (vespene_cost > 0 >= state.vps):
            return False
        return True

    def simulate_economy_until_item_can_be_build(self, item, state: VirtualGameState):
        """
        Simulates economy state in virutal game state object until object can start to be build/researched

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        crystal_cost, vespene_cost = self.sc2_helper.get_resources_costs(item)
        needed_crystal = crystal_cost - state.crystals
        needed_vespene = vespene_cost - state.vespene
        t1 = t2 = 0
        if state.cps > 0:
            t1 = needed_crystal / state.cps
        if state.vps > 0:
            t2 = needed_vespene / state.vps
        t = max(t1, t2)
        state.crystals += state.cps * t
        state.vespene += state.vps * t
        state.timer += t

    def start_building(self, item, state: VirtualGameState):
        """
        Puts construction of an item on the queue.
        Manages economy state in virtual game state associated with current solution as well calls function that
        manage army slots, tracking count of buildings and tracks researched upgrades

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        crystal_cost, vespene_cost = self.sc2_helper.get_resources_costs(item)
        build_time = self.sc2_helper.get_unit_time_value(item)
        state.crystals -= crystal_cost
        state.vespene -= vespene_cost
        state.queue.append(
            QueueBuilding(item, state.timer + build_time)
        )
        self.pre_creation_manage_cps_vps(item, state)
        self.pre_manage_army(item, state)
        self.pre_manage_buildings(item, state)
        self.pre_manage_upgrades(item, state)

    def pre_manage_upgrades(self, item, state: VirtualGameState):
        """
        If item is of type ZergUpgrades.Upgrades it add it to researched upgrades in state object

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        if isinstance(item, ZergUpgrades.Upgrades):
            state.add_upgrade(item)

    def pop_next_created_item(self, state: VirtualGameState):
        """
        Method to get next item that would be create in game from queue in virtual game state.
        If that queue is empty it throws InvalidBuildOrderException

        :param state: Virtual game state of the current Gene
        :return: Next item to be created
        :rtype: QueueBuilding
        """
        if len(state.queue) == 0:
            raise InvalidBuildOrderException
        min_item = min(state.queue)
        state.queue.pop(state.queue.index(min_item))
        return min_item

    def simulate_economy_until_time(self, end_time, state: VirtualGameState):
        """
        Simulates economy of the bot until given time

        :param end_time: New time in which state should be found
        :param state: Virtual game state of the current Gene
        """
        t = end_time - state.timer
        state.crystals += state.cps * t
        state.vespene += state.vps * t
        state.timer += t

    def manage_created_item(self, queue_item: QueueBuilding, state: VirtualGameState):
        """
        Manages created item from the queue. Adds fitness value of created item to the state.
        If created item is building then adds it to current created buildings in virtual game state.
        Calls method that manages army units

        :param queue_item: Item that was created
        :param state: Virtual game state of the current Gene
        """
        item = queue_item.unit
        state.fit_value += self.get_fitness_value_of_item(item, state)
        if isinstance(item, ZergBuildings.Buildings):
            state.buildings.add(item.type_id)
        self.manage_army_unit_added(item, state)

    def manage_army_unit_added(self, item, state: VirtualGameState):
        """
        Method that invokes other methods that involves management of army status

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        self.post_manage_army_slots(item, state)
        self.post_creation_manage_cps_vps(item, state)
        self.post_manage_drone_state(item, state)

    def pre_manage_army_slots(self, item, state: VirtualGameState):
        """
        Manages army slots avaiable in virtual game state on start of building process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        if isinstance(item, ZergBuildings.Buildings):
            if item not in ZergBuildings.evolution_building_set:
                state.slots += 1
        if isinstance(item, ZergUnits.Units):
            state.slots -= self.sc2_helper.get_unit_army_slots(item)

    def pre_creation_manage_cps_vps(self, item, state: VirtualGameState):
        """
        Manages economy status on start of building process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        if isinstance(item, ZergBuildings.Buildings):
            if state.vps > 0:
                state.vps -= 4
                state.missing_vespene_workers_count += 1
            else:
                state.cps -= 8
                state.missing_crystal_workers_count += 1

    def pre_manage_drone_state(self, item, state):
        """
        Manages drone state on start of building process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        if isinstance(item, ZergBuildings.Buildings):
            state.drones -= 1

    def post_creation_manage_cps_vps(self, item, state: VirtualGameState):
        """
        Manages economy status after creation of item

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        if item == ZergUnits.Units.DRONE:
            if state.missing_crystal_workers_count > 0:
                state.cps += 8
                state.missing_crystal_workers_count -= 1
            elif state.missing_vespene_workers_count > 0:
                state.vps += 4
                state.missing_vespene_workers_count -= 1

    def post_manage_army_slots(self, item, state: VirtualGameState):
        """
        Manages slots in army after creation of item.
        Throws InvalidBuildOrderException upon exceeding 207 army slots ( last overlord can be built with 198 slots
        adding only 2 slots, but for easier calculation 207 is used)

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        if item == ZergUnits.Units.OVERLORD:
            state.slots += 8
            if state.army_limit + 8 <= 207:
                state.army_limit += 8
            else:
                raise InvalidBuildOrderException()

    def post_manage_drone_state(self, item, state):
        """
        Manages state of drones and required workers count after item was created.
        Throws InvalidBuildOrderException if drones would exceed 70 drones.

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        if item == ZergUnits.Units.DRONE:
            if state.drones > 70:
                raise InvalidBuildOrderException
            state.drones += 1
        if item == ZergBuildings.Buildings.HATCHERY:
            state.ideal_drone_count += 16
            state.missing_crystal_workers_count += 16
        if item == ZergBuildings.Buildings.EXTRACTOR:
            state.ideal_drone_count += 3
            state.missing_vespene_workers_count += 3

    def get_fitness_value_of_item(self, item, state: VirtualGameState):
        """
        Returns fitness value of and item depending on the type of item

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        :return: Fitness value of item passed in method
        """
        if isinstance(item, ZergUnits.Units):
            return self.get_fitness_value_of_unit(item, state)
        elif isinstance(item, ZergBuildings.Buildings):
            return self.get_fitness_value_of_building(item, state)
        elif isinstance(item, ZergUpgrades.Upgrades):
            return self.get_fitness_value_of_upgrade(item, state)
        return state.get_unit_fitness_value(item)

    def get_fitness_value_of_building(self, building: ZergBuildings.Buildings, state: VirtualGameState):
        """
        Returns fitness value of a building in current state of the game

        :param building: Enum of type ZergBuildings.Buildings
        :param state: Virtual game state of the current Gene
        :return: Fitness value of a building
        """
        if building == ZergBuildings.Buildings.HATCHERY:
            value = state.get_unit_fitness_value(building)
            value *= state.timer / 60 - (3 * state.townhalls) + 1
        return state.get_unit_fitness_value(building)

    def get_fitness_value_of_unit(self, unit : ZergUnits.Units, state: VirtualGameState):
        """
        Returns fitness value of an army unit ion current state of the game

        :param unit: Enum of type ZergUnits.Units
        :param state: Virtual game state of the current Gene
        :return: Fitness value of an army unit
        """
        if unit == ZergUnits.Units.DRONE:
            return state.get_unit_fitness_value(unit) + (state.ideal_drone_count -
                                                         state.drones)
        elif unit == ZergUnits.Units.OVERLORD:
            if state.slots <= 8:
                return state.get_unit_fitness_value(unit)
            raise InvalidBuildOrderException  # will it help?
        elif unit == ZergUnits.Units.QUEEN:
            return state.get_unit_fitness_value(unit)
        if state.under_attack:
            return state.get_unit_fitness_value(unit) * 5
        else:
            return state.get_unit_fitness_value(unit)

    def pre_manage_army(self, item, state):
        """
        Calls army management methods during start of building process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        if item == ZergUnits.Units.QUEEN:
            if state.queens == state.townhalls + 1:
                raise InvalidBuildOrderException
        self.pre_manage_army_slots(item, state)
        self.pre_manage_army_counts(item, state)

    def pre_manage_buildings(self, item, state):
        """
        Manges count of buildings during start of building process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state: Virtual game state of the current Gene
        """
        if isinstance(item, ZergBuildings.Buildings):
            if item == ZergBuildings.Buildings.EXTRACTOR:
                state.vespene_extractors += 1
            if item == ZergBuildings.Buildings.HATCHERY:
                state.townhalls += 1

    def get_fitness_value_of_upgrade(self, upgrade: ZergUpgrades.Upgrades, state: VirtualGameState):
        """
        Returns fitness value of an ZergUpgrades.Upgrade enum

        :param upgrade: Enum of type ZergUpgrades.Upgrades
        :param state:Virtual game state of the current Gene
        :return: Fitness value of and upgrade
        """
        affected_units = self.sc2_helper.get_affected_units(upgrade)
        value = 0
        for unit in affected_units:
            count = state.get_count_of_army_unit(unit)
            value += count * state.get_unit_fitness_value(unit)
        value *= state.get_unit_fitness_value(upgrade)
        if value < 300:
            raise InvalidBuildOrderException
        return value

    def pre_manage_army_counts(self, item, state: VirtualGameState):
        """
        Method that manages count of units on start of creation process

        :param item: ZergUnits.Units, ZergBuildings.Buildings or ZergUpgrades.Upgrades enum
        :param state:  Virtual game state of the current Gene
        """
        if item == ZergUnits.Units.QUEEN:
            state.queens += 1
        if item == ZergUnits.Units.BANELING:
            state.zerglings -= 1
        if item == ZergUnits.Units.ZERGLING:
            state.zerglings += 2
            state.add_unit(item)
        if item == ZergUnits.Units.BROOD_LORD:
            state.corruptor -= 1
        state.add_unit(item)

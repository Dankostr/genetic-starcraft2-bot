from functools import total_ordering


@total_ordering
class QueueBuilding(object):

    def __init__(self, item, end_time):
        """
        Constructor for object to imitate building of the item

        :param item: Item to be built
        :param end_time: Time in which it will end constructing
        """
        self.unit = item
        self.end_time = end_time

    def get_end_time(self):
        """
        Return end time in seconds when the item will be created

        :return: Time in seconds in which item will be created
        """
        return self.end_time

    def __eq__(self, other):
        return self.end_time == other.end_time

    def __ge__(self, other):
        return self.end_time > other.end_time
class InvalidBuildOrderException(Exception):
    """
    Exception that should be thrown when build order encoded in Gene does not meet rules stated by fitness function
    """
    pass

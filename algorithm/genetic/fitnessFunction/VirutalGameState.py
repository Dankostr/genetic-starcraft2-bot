from starcraft.GameStateSnapshot import GameStateSnapshot
from starcraft.ZergValuesSnapshot import ZergValuesSnapshot


class VirtualGameState(object):

    def __init__(self, game_state: GameStateSnapshot, zerg_values: ZergValuesSnapshot):
        """
        Constructor for an object that imitates state of the game.
        It takes current Game State snapshot as well as snapshot of fitness values

        :param game_state: Snapshot of the current game state
        :param zerg_values: Snapshot of the current fitness values
        """

        self.buildings = set(game_state.get_buildings())
        self.townhalls = game_state.get_hatcheries_count()
        self.vespene_extractors = game_state.get_vespene_extractors_count()
        self.queens = game_state.get_queen_count()
        self.zerglings = game_state.get_zergling_count()
        self.corruptor = game_state.get_corruptor_count()
        self.queue = []
        self.timer = game_state.get_timer()
        self.cps = game_state.get_mineral_per_minute() / 60
        self.vps = game_state.get_vespene_per_minute() / 60
        self.crystals = game_state.get_minerals_count()
        self.vespene = game_state.get_vespene_count()
        self.slots = game_state.get_army_free_slots()
        self.army_limit = game_state.get_army_limit()
        self.drones = game_state.get_drones_count()
        self.ideal_drone_count = game_state.get_ideal_worker_count()
        self.missing_crystal_workers_count = game_state.get_missing_crystal_workers()
        self.missing_vespene_workers_count = game_state.get_missing_vespene_workers()
        self.fit_value = 0
        self.zerg_values = zerg_values
        self.under_attack = game_state.is_under_attack()
        self.units = game_state.get_units()
        self.upgrades = game_state.get_current_upgrades()

    def is_item_in_queue(self, item):
        """
        Check if given item is under building in virtual game state

        :param item: ZergUnits.Units, ZergBuildings.Buildings, ZergUpgrades.Upgrades
        :return: Boolean indicating if item is in queue
        """
        if item in list(map(lambda x: x.unit, self.queue)):
            return True
        return False

    def get_unit_fitness_value(self, item):
        """
        Return fitness value for given item

        :param item: Enum from ZergUnits.Units, ZergBuildings.Buildings, ZergUpgrades.Upgrades
        :return: Int value representing fitness value of an item
        """
        return self.zerg_values.get_unit_fitness_value(item)

    def get_count_of_army_unit(self, unit):
        """
        Return how much units of given type are  in army

        :param unit: ZergUnits.Units object to be counted
        :return: Amount of units of given type
        """
        return len(list(filter(lambda x: x == unit.type_id, self.units)))

    def add_unit(self, unit):
        """
        Add unit to the list of army units

        :param unit: ZergUnits.Units object to be added to list
        """
        self.units.append(unit)

    def add_upgrade(self, upgrade):
        """
        Add upgrade to the set of upgrades

        :param upgrade: ZergUpgrades.Upgrades object to be added to set
        """
        self.upgrades.add(upgrade)

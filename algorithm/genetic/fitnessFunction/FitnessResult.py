from cmath import inf
from functools import total_ordering

from config import Config


@total_ordering
class FitnessResult(object):

    def __init__(self, value, length, time, crystal, vespene, cps, vps):
        """
        Object that represent value of an gene

        :param value: Value calculated during work of fitness function
        :param length: Length of the solution
        :param time: Time in which solution will end
        :param crystal: Crystal count
        :param vespene: Vespene count
        :param cps: Crystals per second
        :param vps: Vespene gas per second
        """
        a = Config().get_solution_factor()
        x0 = Config().get_solution_min_length()
        x1 = Config().get_solution_max_length()
        self.value = max(0, a * (length - x0) * (length - x1)) * value
        self.time = time
        self.crystal = crystal
        self.vespene = vespene
        self.cps = cps
        self.vps = vps

    def __eq__(self, other):
        if self.value == other.value and self.time == other.time and self.crystal == other.crystal and self.vespene \
                == other.vespene:
            return True
        return False

    def __ge__(self, other):
        if self.value > other.value:
            return True
        elif self.value == other.value:
            if self.time == other.time:
                if self.crystal > other.crystal:
                    return True
                if self.vespene > other.vespene:
                    return True
            elif self.time < other.time:
                t = other.time - self.time
                if self.crystal + t * self.cps > other.crystal:
                    return True
                if self.vespene + t * self.vps > other.vespene:
                    return True
            elif self.time > other.time:
                t = other.time - self.time
                if self.crystal > other.crystal + t * other.cps:
                    return True
                if self.vespene > other.vespene + t * other.vps:
                    return True
        return False

    def __repr__(self):
        return f"Value:{self.value}\n Time:{self.time}\n Crystal:" \
               f"{self.crystal} \n , Vespene: {self.vespene} \n"

    @staticmethod
    def invalid_build_order():
        """
        Generated result for invalid build order

        :return: FitnessResult object with -inf in all fields
        """
        result = FitnessResult(-inf, -inf, -inf, -inf, -inf, -inf, -inf)
        result.value = -inf
        result.time = inf
        result.crystal = -inf
        result.vespene = - inf
        result.cps = -inf
        result.vps = -inf
        return result

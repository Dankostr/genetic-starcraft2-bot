import random
from typing import List

from algorithm.genetic.Gene import Gene
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class GeneticAlgorithm(object):

    @staticmethod
    def cross_in_population(genes: List[Gene]) -> Gene:
        """
        Method to perform cross on 2 genes from the  list of genes

        :param genes: List of genes
        :return: New Gene object with new solution
        """
        g1, g2 = random.sample(genes, 2)
        return GeneticAlgorithm.crossover_genes(g1, g2)

    @staticmethod
    def cross_between_populations(first_population: List[Gene], second_population: List[Gene]) ->\
            Gene:
        """
        Method to perform on 2 genes from 2 different list of genes

        :param first_population: List of genes
        :param second_population: List of genes
        :return: New Gene object with new solution
        """
        g1 = random.choice(first_population)
        g2 = random.choice(second_population)
        g1, g2 = GeneticAlgorithm._randomize_gene_position(g1, g2)
        return GeneticAlgorithm.crossover_genes(g1, g2)

    @staticmethod
    def crossover_genes(g1: Gene, g2: Gene) -> Gene:
        """
        Method to crossover genes

        :param g1: Gene
        :param g2: Gene
        :return: new Gene with solution created from g1 and g2
        """
        lo1 = len(g1.get_build_order())
        lo2 = len(g2.get_build_order())
        order = g1.get_build_order()[0:random.randint(1, lo1 - 1)] + g1.get_build_order()[
                                                                     0:random.randint(1, lo2 - 1)]
        return Gene(order=order)

    @staticmethod
    def mutate(genes: List[Gene]) -> Gene:
        """
        Method to perform mutation on a Gene from list of Genes

        :param genes: List of genes
        :return: new Gene object on which mutation was performed
        """
        unit_list = ZergBuildings.building_list[:]
        unit_list.extend(ZergUnits.units_list)
        o1 = random.choice(genes)
        lo1 = len(o1.get_build_order())
        item_index = random.randint(0, lo1 - 1)
        new_order = o1.get_build_order()[:]
        new_order[item_index] = random.choice(unit_list)
        return Gene(order=new_order)

    @staticmethod
    def _randomize_gene_position(g1: Gene, g2: Gene):
        """
        Randomize genes position in which they will be arranged

        :param g1: Gene
        :param g2: Gene
        :return: g1 and g2 are returned in random order
        """
        if random.random() <= 0.5:
            return g1, g2
        return g2, g1

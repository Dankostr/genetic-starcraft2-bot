import random

from config import Config
from starcraft.GameState import GameState
from starcraft.wrapper.SC2Helper import SC2Helper
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUpgrades import ZergUpgrades


class Gene(object):

    def __init__(self, order=None):
        """
        Create gene.
        If order parameter is empty try to generate semi valid build order, otherwise save order as new build order

        :param order: Optional parameter that accepts build orders and saves them in Gene
        """
        if order is None:
            self.build_order: list = Gene.__generate_semi_valid_build_order()
        else:
            self.build_order = order

    def shorten_gene_length_to(self, length):
        """
        Shorten the length of gene to specified length

        :param length: Length to which gene should be shortened
        """
        self.build_order = self.build_order[0:min(len(self.build_order), length)]

    def get_build_order(self):
        """
        Returns list which contains build order

        :return: List with build order
        """
        return self.build_order

    def __str__(self):
        return str(self.build_order)

    @staticmethod
    def __generate_semi_valid_build_order() -> list:
        """
        Method to generate semi valid build orders from current state of the game

        :return: Newly created semi valid build order
        """
        current_buildings = set(GameState().snapshot().get_buildings())
        current_upgrades = set(GameState().snapshot().get_current_upgrades())
        timer = GameState().snapshot().get_timer()
        build_order = []
        for i in range(Config().get_solution_optimal_length()):
            to_create = Gene.__get_all_that_can_be_created(current_buildings)
            to_create = Gene.__filter_available_buildings(to_create, current_buildings)
            to_create = Gene.__filter_upgrades(to_create, current_upgrades, timer)
            next_item = random.choice(to_create)
            build_order.append(next_item)
            if isinstance(next_item, ZergBuildings.Buildings):
                current_buildings.add(next_item.type_id)
        return build_order

    @staticmethod
    def __get_all_that_can_be_created(current_buildings):
        """
        Method to retrieve all units, buildings and upgrade that can be created/researched in current state of the game

        :param current_buildings: Set of currently created buildings in game
        :return: List of all items that can be created/researched
        """
        available_buildings = []
        zerg_dependencies = SC2Helper().get_dependencies()
        for unit in zerg_dependencies.keys():
            unit_dependencies = set(map(lambda x: x.type_id, zerg_dependencies[unit]))
            if unit_dependencies.issubset(current_buildings):
                available_buildings.append(unit)
        return available_buildings

    @staticmethod
    def __filter_available_buildings(to_create, current_buildings):
        """
        Filter buildings that are currently built in game, and should not be built twice.

        :param to_create: Item that can be created
        :param current_buildings: Set of currently created buildings in game
        :return: List of items that can be created in game without buildings that should not be created twice.
        """
        return list(filter(
            lambda x: x.type_id not in current_buildings or x.type_id == ZergBuildings.Buildings.HATCHERY.type_id or
                      x.type_id == ZergBuildings.Buildings.EXTRACTOR.type_id,
            to_create))

    @staticmethod
    def __filter_upgrades(to_create, current_upgrades, timer):
        """
        Filter upgrades that are currently researched in game.

        :param to_create: Item that can be created
        :param current_upgrades: Set of currently researched upgrades in game
        :return: List of items that can be created in game without upgrades that are currently researched in game.
        """
        return list(filter(lambda x: Gene.__filter_for_upgrades(x, current_upgrades, timer), to_create))

    @staticmethod
    def __filter_for_upgrades(item, current_upgrades, timer):
        """
        Filter that checks if item is and upgrade and if it is check if it should be filtered out.
        Filtered upgrades are that, which has been currently researched in game.
        Method can be set to filter out all upgrades in configuration

        :param item: Item that can be created/researched in game
        :param current_upgrades: Set of current upgrades
        :param timer: Current time in Game
        :return: boolean value if upgrade can be researched or not.
        """
        if isinstance(item, ZergUpgrades.Upgrades):
            if not Config().use_upgrades():
                return False
            if item.type_id not in current_upgrades and timer / 60 > 19:
                return True
            return False
        return True

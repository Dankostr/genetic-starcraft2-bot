from sc2.units import Units

from starcraft.wrapper.SC2Helper import SC2Helper
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits
from starcraft.wrapper.ZergUpgrades import ZergUpgrades


class GameStateSnapshot(object):

    def __init__(self, minerals: int, vespene: int, drones_count: int, townhalls: Units,
                 vespene_extractors: Units,
                 units: Units,
                 buildings: Units, army_limit: int, army_size: int, minerals_per_minute: int,
                 vespene_per_minute: int,
                 buildings_in_construction: list,
                 build_order: list,
                 ideal_worker_count: int,
                 missing_crystal_workers_count: int,
                 missing_vespene_workers_count: int,
                 timer: float,
                 queen_in_queue: int,
                 under_attack: bool,
                 upgrades: set,
                 overlords_in_building: int):
        """
        Constructor for GameStateSnapshot object, that should be used to create snapshot of game state.

        :param minerals:
        :param vespene:
        :param drones_count:
        :param townhalls:
        :param vespene_extractors:
        :param units:
        :param buildings:
        :param army_limit:
        :param army_size:
        :param minerals_per_minute:
        :param vespene_per_minute:
        :param buildings_in_construction:
        :param build_order:
        :param ideal_worker_count:
        :param missing_crystal_workers_count:
        :param missing_vespene_workers_count:
        :param timer:
        :param queen_in_queue:
        :param under_attack:
        :param upgrades:
        :param overlords_in_building:
        """
        self.minerals = minerals
        self.vespene = vespene
        self.drones_count = drones_count
        self.townhalls = list(map(lambda unit: unit.type_id, townhalls))
        self.vespene_extractors = list(map(lambda unit: unit.type_id, vespene_extractors))
        self.units = self._map_to_type_id(units)
        self.buildings = list(map(lambda unit: unit.type_id, buildings))
        self.army_limit = army_limit
        self.army_size = army_size
        self.minerals_per_minute = minerals_per_minute
        self.vespene_per_minute = vespene_per_minute
        self.buildings_in_construction = buildings_in_construction
        self.build_order_buildings = self._get_build_order_buildings(build_order)
        self.build_order_units = self._get_build_order_units(build_order)
        self.ideal_worker_count = ideal_worker_count
        self.build_order_upgrades = self._get_build_order_upgrades(build_order)
        self.missing_crystal_workers_count, self.missing_vespene_workers_count = self._get_missing_resource_workers_count(
            missing_crystal_workers_count,
            missing_vespene_workers_count)
        self.timer = timer
        self.queen_in_queue = queen_in_queue
        self.under_attack = under_attack
        self.current_upgrades = upgrades
        self.overlords_in_building = overlords_in_building

    def get_drones_count(self) -> int:
        """
        Method to calculate current drone count

        :return: amount of drones currently in game
        """
        return self.drones_count + self._get_drones_in_build_order()

    def _get_drones_in_build_order(self):
        """
        Method to return amount of drones in current build order

        :return: amount of drones in build order
        """
        return len(list(filter(lambda x: x == ZergUnits.Units.DRONE,
                               self.build_order_units)))

    def get_mineral_per_minute(self) -> int:
        """
        Method to return rate of mining minerals per minute

        :return: Amount of minerals mined per minute
        """
        return self.minerals_per_minute

    def get_minerals_count(self) -> int:
        """
        Method to return current mineral count

        :return: Amount of minerals
        """
        return self.minerals

    def get_vespene_per_minute(self) -> int:
        """
         Method to return rate of mining vespene per minute

         :return: Amount of vespene gathered per minute
         """
        return self.vespene_per_minute

    def get_vespene_count(self) -> int:
        """
        Method to return current vespene gas count

        :return: Amount of vespene gas
        """
        return self.vespene

    def get_hatcheries_count(self) -> int:
        """
        Returns current amount of hatcheries in game

        :return: Amount of hatcheries in game
        """
        return len(self.townhalls) + self.get_build_order_hatcheries_count()

    def get_buildings(self) -> list:
        """
        Return list of all buildings currently in game

        :return: list of all buildings currently in game
        """
        return self.buildings + self.buildings_in_construction + self._map_to_type_id(
            self.build_order_buildings)

    def get_army_limit(self) -> int:
        """
        Return value of army limit

        :return: Int value of army limit
        """
        return self.army_limit + self.get_build_order_additional_army_limit() + 8 * self.overlords_in_building

    def get_army_size(self) -> int:
        """
        Return value of army size

        :return: Int value of army size
        """
        return self.army_size + self._get_build_order_army_size() + 2 * self.queen_in_queue

    def get_army_free_slots(self) -> int:
        """
        Return how many free slots army has

        :return: Int value of free army slots
        """
        return self.get_army_limit() - self.get_army_size()

    def get_vespene_extractors_count(self) -> int:
        """
        Return amount of vespene extractors

        :return: amount of vespene extractors
        """
        return len(self.vespene_extractors)

    def get_units(self) -> list:
        """
        Return list of all currently possesed units in game

        :return: List of bot's all units
        """
        return self.units + self._map_to_type_id(self.build_order_units)

    def get_queen_count(self) -> int:
        """
        Return amount of queens currently in game

        :return: Amount of queens in game
        """
        c = 0
        for unit in self.units + self._map_to_type_id(self.build_order_units):
            if unit == ZergUnits.Units.QUEEN.type_id:
                c += 1
        return c + self.queen_in_queue

    @staticmethod
    def _map_to_type_id(list_to_map) -> list:
        """
        Maps list of objects ( ZergUnits.Units, ZergBuildings.Buildings, ZergUpgrades.Upgrades, Units) to their type_id

        :param list_to_map: List of object to map
        :return: List of type_id of objects
        """
        return list(map(lambda x: x.type_id, list_to_map))

    @staticmethod
    def _get_build_order_units(build_order):
        """
        Return list of units that are currently in build order waiting to be built

        :param build_order: current build order
        :return: List of units that are currently in build order
        """
        return list(filter(lambda x: isinstance(x, ZergUnits.Units), build_order))

    @staticmethod
    def _get_build_order_buildings(build_order):
        """
        Return list of buildings that are currently in build order waiting to be built

        :param build_order: current build order
        :return: List of buildings that are currently in build order
        """
        return list(filter(lambda x: isinstance(x, ZergBuildings.Buildings), build_order))

    def _get_build_order_army_size(self):
        """
        Return how many army slots will be taken by units in build order

        :return: Amount of slots taken by army in build order
        """
        size = 0
        for unit in self.build_order_units:
            size += SC2Helper().get_unit_army_slots(unit)
        return size - len(list(filter(lambda x: x.type_id != ZergBuildings.Buildings.LAIR and x.type_id !=
                                                ZergBuildings.Buildings.HIVE and x.type_id !=
                                                ZergBuildings.Buildings.GREATER_SPIRE,
                                      self.build_order_buildings)))

    def get_build_order_additional_army_limit(self):
        """
        Return how many slots will be added by overlords in build order

        :return: Amount of slots added by overlords in build order
        """
        limit = 0
        for unit in self.build_order_units:
            if unit == ZergUnits.Units.OVERLORD:
                limit += 8
        return limit

    def get_ideal_worker_count(self):
        """
        Return amount of workerss to saturate all mining buildings

        :return: Amount of workers to saturate all mining buildings
        """
        return self.ideal_worker_count

    def get_missing_crystal_workers(self):
        """
        Return amount of missing crystal workers to saturate all mining fields

        :return: mount of missing crystal workers to saturate all mining fields
        """
        return self.missing_crystal_workers_count

    def get_missing_vespene_workers(self):
        """
        Return amount of missing vespene workers to saturate all geysers

        :return: mount of missing vespene workers to saturate all geysers
        """
        return self.missing_vespene_workers_count

    def _get_missing_resource_workers_count(self, missing_crystal_workers_count,
                                            missing_vespene_workers_count):
        """
        Calculates how many workers are missing for mineral fields and geysers including drones in build order

        :param missing_crystal_workers_count:  current missing crystal workers in game
        :param missing_vespene_workers_count: current missing vespene workers in game
        :return: Return 2 int values, first describing amount of missing crystal workers, second amount of missing
        vespene workers
        """
        drones_in_building = self._get_drones_in_build_order()
        if drones_in_building > 0:
            m = min(drones_in_building, missing_crystal_workers_count)
            missing_crystal_workers_count -= m
            drones_in_building -= m
        if drones_in_building > 0:
            m = min(drones_in_building, missing_vespene_workers_count)
            missing_vespene_workers_count -= m
            drones_in_building -= m
        return missing_crystal_workers_count, missing_vespene_workers_count

    def get_timer(self) -> float:
        """
        Return current time in game

        :return: Current time in game
        """
        return self.timer

    def get_zergling_count(self):
        """
        Return amount of zerglings in army

        :return: Amount of zerglings in army
        """
        c = 0
        for unit in self.units + self._map_to_type_id(self.build_order_units):
            if unit == ZergUnits.Units.ZERGLING.type_id:
                c += 1
        return c

    def get_corruptor_count(self):
        """
        Return amount of corruptor in army

        :return: Amount of corruptor in army
        """
        c = 0
        for unit in self.units + self._map_to_type_id(self.build_order_units):
            if unit == ZergUnits.Units.CORRUPTOR.type_id:
                c += 1
        return c

    def is_under_attack(self):
        """
        Return if base is under attack

        :return: boolean value indicating if base is under attack
        """
        return self.under_attack

    def get_current_upgrades(self):
        """
        Return set of current upgrades researched by bot

        :return: Set of current upgrades researched by bot
        """
        return self.current_upgrades | self.build_order_upgrades

    @staticmethod
    def _get_build_order_upgrades(build_order):
        """
        Return list of upgrades that are currently in build order waiting to be researched

         :param build_order: current build order
         :return: List of upgrades that are currently in build order
        """
        return set(filter(lambda x: isinstance(x, ZergUpgrades.Upgrades), build_order))

    def get_build_order_hatcheries_count(self):
        """
        Return amount of hatcheries that are currently in  build order waiting to be created

        :return: Amount of hatcheries that are currently in build order
        """
        c = 0
        for building in self.build_order_buildings:
            if building == ZergBuildings.Buildings.HATCHERY:
                c += 1
        return c

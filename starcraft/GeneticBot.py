import itertools
import threading
from collections.abc import Set
from queue import Queue, Empty
from typing import Optional, Union

import sc2
from loguru import logger
from sc2 import AbilityId, UnitTypeId
from sc2.dicts.unit_research_abilities import RESEARCH_INFO
from sc2.dicts.upgrade_researched_from import UPGRADE_RESEARCHED_FROM
from sc2.ids.buff_id import BuffId
from sc2.ids.upgrade_id import UpgradeId
from sc2.position import Point2
from sc2.unit import Unit
from sc2.units import Units

from starcraft.GameState import GameState
from starcraft.ZergValues import ZergValues
from starcraft.wrapper.ProtossUnits import ProtossUnits
from starcraft.wrapper.SC2Helper import SC2Helper
from starcraft.wrapper.TerranUnits import TerranUnits
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class GeneticBot(sc2.BotAI):
    """Class extending base class used for bot creation"""
    def __init__(self, get_build_order_event: threading.Event, queue: Queue,
                 start_calculating_event: threading.Event):
        """
            Constructor for the bot class, responsible for playing the game.

        :param get_build_order_event: is threading.Event object that is used to request build order from genetic
        algorithm
        :param queue: Queue object on which genetic algorithm puts solutions
        :param start_calculating_event: threading.Event object to resume calculations in genetic algorithm
        """
        super().__init__()
        self.overlords_in_building = 0
        self.under_attack = False
        self.scouting_table = [1, 7, 13, 19, 24]
        self.game_state = GameState()
        self.get_build_order_event = get_build_order_event
        self.start_calculating_event = start_calculating_event
        self.zerg_values = ZergValues()
        self.queue = queue
        self.found_enemy_units = dict()
        self.path = []
        self.buildings_in_constructions = []
        self.sc2_helper = SC2Helper()
        self.attack_timer_multiplication = 6
        self.scouting_overlord_tag = -1
        self.group_attacking = Units([], self)
        self.gathering_point: Point2 = None
        self.ordered_expansions = None
        self.clear_map = None
        self.target = None
        self.main_base = None
        self.queens_in_queue = 0
        self.last_time_created = 0
        self.upgrades = set()

    async def on_before_start(self):
        """
        This method is called before "on_start". It requests new build order from algorithm.
        and before "prepare_first_step" . Refer to documentation of sc2.BotAi for more information
        """
        self.get_new_build_order()

    async def on_start(self):
        """
        This method is called before game. It finds fids first gathering point for allied units and find first
        enemy base.
        Refer to documnetation of sc2.BotAI for more information.
        """
        self.gathering_point = self.main_base_ramp.top_center
        self.ordered_expansions = sorted(
            self.expansion_locations_list, key=lambda expansion: expansion.distance_to(self.enemy_start_locations[0])
        )
        self.clear_map = itertools.cycle(self.ordered_expansions)
        self.target = next(self.clear_map)
        self.main_base = self.townhalls.first

    async def on_step(self, iteration: int):
        """
        This method is called on every game step (looped in realtime mode).
        It is responsible for calling bot main routines like : metaheuristic_loop(), queen_management(),
        drones_management(), scouting_management() and attack_management()

        :param iteration: Providing step number
        """
        await self.metaheuristic_loop()
        await self.queen_management()
        await self.drones_management()
        await self.scouting_management()
        await self.attack_management()

    async def metaheuristic_loop(self):
        """
        Method providing logic for creation of units and buildings during game by using solutions found by genetic
        algorithm. It is called on every step of the game. When current solution is empty, it request new solution.
        """
        self.update_game_state()
        is_started = False
        if len(self.path) > 0:
            if self.last_time_created + 90 >= self.time:
                unit = self.path[0]
                if self.can_afford(unit.value) and await self.are_dependencies_ready(unit):
                    if isinstance(unit, ZergUnits.Units):
                        is_started = await self.create_unit(unit)
                    elif isinstance(unit, ZergBuildings.Buildings):
                        is_started = await self.create_building(unit)
                    else:
                        is_started = await self.research_upgrade(unit)
                if is_started:
                    self.last_time_created = self.time
                    await self.pop_created_item()
            else:
                logger.error(self.path)
                self.path = []
                self.buildings_in_constructions = []
                self.last_time_created = self.time
        else:
            self.get_new_build_order()

    async def create_unit(self, unit: ZergUnits.Units) -> bool:
        """
        Method responsible for creating army unit. Checks
        whether  unit necessary for creation is available. Includes special handlers for queens, infestors,
        swarm_host , viper as well as units that evolve from other units that can be fighting.

        :param unit: Enum from ZergUnits.Units enum , which describe army unit that should be created
        :return: Boolean that indicated whether creation was sucessful or not.
        """
        if self.larva.amount:
            if self.train(unit.type_id) != 0:
                if unit == ZergUnits.Units.OVERLORD:
                    self.overlords_in_building += 1
                return True
            else:
                if unit == ZergUnits.Units.QUEEN:
                    self.townhalls.first.train(unit.type_id, queue=True)
                    self.queens_in_queue += 1
                    return True
                elif unit == ZergUnits.Units.BANELING:
                    if await self.create_morphing_unit(ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING):
                        return True
                elif unit == ZergUnits.Units.BROOD_LORD:
                    if await self.create_morphing_unit(ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD):
                        return True
                elif unit == ZergUnits.Units.INFESTOR:
                    return True
                elif unit == ZergUnits.Units.SWARM_HOST:
                    return True
                elif unit == ZergUnits.Units.VIPER:
                    return True
        return False

    async def create_building(self, building) -> bool:
        """
        Method responsible for creating buildings. Contains handlers for creating extractors hatcheries, lairs,
        hives and greater spires. It finds position on which given buildings should be created, then chooses
        appropriate worker to create this building.

        :param building: ZergBuildings.Buildings enum that represents building for Zerg race
        :return: Boolean representing if operation of creation was successful
        """
        started = False
        if building == ZergBuildings.Buildings.EXTRACTOR:
            vg = await self.get_available_vespene_geyser()
            if self.townhalls.amount * 2 == self.gas_buildings.amount:
                return True
            if self.workers.amount >= 0 and vg is not None:
                self.workers.filter(lambda x: x.is_gathering).random.build_gas(vg)
                started = True
            else:
                return False

        elif building == ZergBuildings.Buildings.HATCHERY:
            planned_hatch_locations: Set = {placeholder.position for placeholder in self.placeholders}
            my_structure_locations: Set = {structure.position for structure in self.structures}
            enemy_structure_locations: Set = {structure.position for structure in self.enemy_structures}
            blocked_locations = my_structure_locations | planned_hatch_locations | enemy_structure_locations
            for location in self.get_expansion_list_in_order():
                if location not in blocked_locations:
                    started = await self.build(building.type_id, near=location, placement_step=1)
                    break

        elif building == ZergBuildings.Buildings.LAIR or building == ZergBuildings.Buildings.HIVE:
            if self.townhalls.filter(lambda x: x.tag == self.main_base.tag).first.is_idle:
                self.main_base.build(building.type_id, queue=True)
                started = True
            else:
                started = False

        elif building == ZergBuildings.Buildings.GREATER_SPIRE:
            spires: Units = self.structures(ZergBuildings.Buildings.SPIRE.type_id).ready
            if spires:
                spires.first.build(building.type_id)
                started = True
        else:
            map_center = self.game_info.map_center
            position_towards_map_center = self.main_base.position.towards(map_center,
                                                                          distance=5)
            position = await self.find_placement(building.type_id, position_towards_map_center)

            if self.workers and position:
                if self.workers.idle:
                    self.workers.idle.closest_to(position).build(building.type_id, position)
                else:
                    worker = await self.get_closest_mineral_worker(position)
                    if worker:
                        worker.build(building.type_id, position)
                    else:
                        self.workers.closest_to(position).build(building.type_id, position)
                started = True

        if started:
            await self.add_building_to_construction_queue(building)
        return started

    async def are_dependencies_ready(self, item):
        """
        Checks if all necessary dependencies for given item are created in game.

        :param item: Enum from ZergBuildings.Buildings, ZergUnits.Units, ZergUpgrades.Upgrades
        :return: Boolean representing if dependencies were created or not.
        """
        unit_dependencies = set(map(lambda x: x.type_id, self.sc2_helper.get_unit_dependencies(
            item)))
        constructed_dependencies = set(map(lambda x: x.type_id, self.structures(
            unit_dependencies).ready))
        return unit_dependencies.issubset(constructed_dependencies)

    async def research_upgrade(self, upgrade):
        """
        Method that research upgrades for bot.

        :param upgrade:  ZergUpgrades.Upgrade enum which represents upgrade in game
        :return:  Boolean stating if research was started.
        """
        started = False
        if await self.are_dependencies_ready(upgrade):
            started = self.research(upgrade.type_id)
            if started:
                self.upgrades.add(upgrade)
        return started

    async def attack_management(self):
        """
        Method responsible for sending and attack and micromanagement.
        It sends units in fixed intervals of time to enemy base location.
        When unit cap is reached it also send units to fight.
        If location does not contain base, it search next possible position for enemy base.
        """
        if self.time > 60 * self.attack_timer_multiplication or self.near_supply_cap():
            self.attack_timer_multiplication += 6
            self.group_attacking = self.get_attacking_group()
            self.send_for_attack(self.target, self.group_attacking)
        if not self.group_attacking.empty:
            if self.get_attacking_group().filter(lambda x: x.can_attack_ground).closer_than(2, self.target):
                self.target = next(self.clear_map)
                while self.target in {structure.position for structure in self.townhalls}:
                    self.target = next(self.clear_map)
            self.send_for_attack(self.target, self.group_attacking)

    async def on_building_construction_complete(self, unit: Unit):
        """
        Event risen when building construction has completed.
        If buildings was tracked during it construction, it is deleting it from tracking.
        After building a new Hatchery it finds new gathering point and sends all units that can fight there.

        :param unit: Unit object that represents object in game
        """
        if unit.type_id in self.buildings_in_constructions:
            self.buildings_in_constructions.remove(unit.type_id)
        if unit.type_id == ZergBuildings.Buildings.HATCHERY.type_id:
            new_gathering_point = min(
                (ramp for ramp in self.game_info.map_ramps if len(ramp.upper) in {2, 5}),
                key=lambda r: unit.distance_to(r.top_center),
            ).bottom_center
            if self.main_base.distance_to(new_gathering_point) < self.main_base.distance_to(self.game_info.map_center):
                self.gathering_point = new_gathering_point
            for attack_unit in self.units.filter(self.is_eligible_to_attack).filter(
                    lambda x: x not in self.group_attacking):
                attack_unit.move(self.gathering_point)

    def send_for_attack(self, target: Point2, units_to_attack: Units):
        """
        Sends command to list of units to attack at given point.

        :param target:  Point2 object that should be attacked
        :param units_to_attack: List of units that should attack at target point
        """
        for unit in units_to_attack:
            unit.attack(target)

    async def add_building_to_construction_queue(self, building: ZergBuildings.Buildings):
        """
        Add building to be tracked by genetic algorithm during construction.

        :param building: ZergBuildings.Buildings enum that represents building that can be created in game
        """
        self.buildings_in_constructions.append(building.type_id)

    async def get_available_vespene_geyser(self) -> Union[Unit, None]:
        """
        Method that search for available geysers around bases and returns unit object if found.

        :return: Unit object, which is geyser or None if geyser not found
        """
        available_geysers = Units([], self)
        my_structure_locations: Set = {structure.position for structure in self.structures} | {placeholder.position for
                                                                                               placeholder in
                                                                                               self.placeholders}
        for townhall in self.townhalls:
            available_geysers += self.vespene_geyser.closer_than(10, townhall).filter(lambda x: x.position not in
                                                                                                my_structure_locations)
        if available_geysers.amount == 0:
            return None
        return available_geysers.first

    async def drones_management(self):
        """
            Method responsible for management of drones.
            It reassigns workers per crystal fields or geysers.
            If currently assigned workers to geysers is more or equal to 18 it does not send them to mine vespene.
        """
        for townhall in self.townhalls.ready:
            await self.__reassign_workers(townhall)
        if (await self.get_workers_gathering_vespene()) <= 18:
            for geyser in self.gas_buildings.ready:
                if self.townhalls.ready:
                    await self.__reassign_workers(geyser)

    async def __reassign_workers(self, mining_building):
        """
        Method that reassign workers to single mining building or stops workers if too many is mining at given
        building.

        :param mining_building: Unit object, which is Base or Geyser
        """
        worker_difference = mining_building.surplus_harvesters
        if worker_difference == 0:
            pass
        elif worker_difference > 0:
            if mining_building.type_id == ZergBuildings.Buildings.EXTRACTOR.type_id:
                local_workers = self.workers.filter(
                    lambda unit: unit.order_target == mining_building.tag
                                 or (unit.is_carrying_vespene and unit.order_target ==
                                     self.townhalls.ready.furthest_to(mining_building).tag)
                )
            else:
                local_minerals_tags = {
                    mineral.tag for mineral in self.mineral_field if
                    mineral.distance_to(mining_building) <= 8
                }
                local_workers = self.workers.filter(
                    lambda unit: unit.order_target in local_minerals_tags
                                 or (
                                         unit.is_carrying_minerals and unit.order_target == mining_building.tag))
            for worker in local_workers.take(worker_difference):
                worker.stop()
        elif worker_difference < 0:
            if self.idle_worker_count > 0:
                for worker in self.workers.idle.take(-worker_difference):
                    if mining_building.type_id == ZergBuildings.Buildings.EXTRACTOR.type_id:
                        worker.gather(mining_building)
                    else:
                        local_minerals = {
                            mineral for mineral in self.mineral_field if
                            mineral.distance_to(mining_building) <= 8
                        }
                        if local_minerals:
                            target_mineral = min(local_minerals,
                                                 key=lambda mineral: mineral.distance_to(worker))
                            worker.gather(target_mineral)

    def update_game_state(self):
        """
        Update game state object which is used to pass information to genetic algorithm
        """
        self.game_state.update_state(
            self.minerals,
            self.vespene,
            self.supply_workers,
            self.townhalls,
            self.gas_buildings,
            self.units,
            self.structures,
            self.supply_cap,
            self.supply_used,
            self.state.score.collection_rate_minerals,
            self.state.score.collection_rate_vespene,
            self.buildings_in_constructions,
            self.path,
            self.get_ideal_workers_count(),
            self.get_missing_crystal_workers_count(),
            self.get_missing_vespene_workers_count(),
            self.time,
            self.queens_in_queue,
            self.under_attack,
            self.upgrades,
            self.overlords_in_building
        )

    def get_new_build_order(self):
        """
        Method that tries to get new solution from algorithm.
        If queue on which solution should be found is empty it is logging warn and tries to get solution on next step.
        """
        self.get_build_order_event.set()
        try:
            self.path = self.queue.get(block=False).get_build_order()
            logger.warning(self.path)
            self.update_game_state()
            self.get_build_order_event.clear()
            self.start_calculating_event.set()
        except Empty:
            logger.warning("empty queue")

    async def queen_management(self):
        """
        Method that send idle queen units to hatcheries which does not have buff placed on them or are not target for
        already moving queens.
        """
        queens = self.units.filter(
            lambda unit: unit.type_id == ZergUnits.Units.QUEEN.type_id and unit.energy >= 25 and not unit.is_attacking)
        queens_order_targets = {q.order_target for q in queens if q.order_target in self.townhalls.ready.tags}
        hatcheries = self.townhalls.filter(
            lambda hatch: not hatch.has_buff(BuffId.QUEENSPAWNLARVATIMER) and hatch.tag not in queens_order_targets
        )
        for queen in queens.filter(lambda q: not q.is_active):
            if not hatcheries.empty:
                hatchery = hatcheries.closest_to(queen)
                queen(AbilityId.EFFECT_INJECTLARVA, hatchery)
                hatcheries.remove(hatchery)

    def get_ideal_workers_count(self) -> int:
        """
        Method that counts how many workers is necessary to saturate all mineral fields and geysers.

        :return: int value representing ideal amount of workers necessary to saturate all mining structures.
        """
        ideal_worker_count = 0
        for townhall in self.townhalls:
            ideal_worker_count += townhall.ideal_harvesters
        for geyser in self.gas_buildings:
            ideal_worker_count += geyser.ideal_harvesters
        return ideal_worker_count

    def get_missing_crystal_workers_count(self) -> int:
        """
        Method that counts how many workers is missing to fully saturate crystal fields.

        :return: int value representing missing amount of workers to fully saturate crystal fields.
        """
        missing_workers = 0
        for townhall in self.townhalls:
            if townhall.surplus_harvesters > 0:
                missing_workers -= townhall.surplus_harvesters
        return missing_workers

    def get_missing_vespene_workers_count(self):
        """
        Method that counts how many workers is missing to fully saturate geysers.

        :return: int value representing missing amount of workers to fully saturate geysers.
        """
        missing_workers = 0
        for vespene_extractor in self.gas_buildings:
            if vespene_extractor.surplus_harvesters > 0:
                missing_workers -= vespene_extractor.surplus_harvesters
        return missing_workers

    async def scouting_management(self):
        """
        Method that sends overlords in fixed time frames to check on enemy structures and army units.
        """
        overlords = self.units.filter(lambda x: x.type_id == ZergUnits.Units.OVERLORD.type_id)
        if overlords.filter(lambda x: not x.is_idle).empty:
            if self.scouting_table:
                if self.time > self.scouting_table[0] * 60 and overlords.amount >= 2:
                    ov = overlords.random
                    self.scouting_table.pop(0)
                    ov.patrol(self.enemy_start_locations[0])
                    self.scouting_overlord_tag = ov.tag

    async def on_enemy_unit_entered_vision(self, unit: Unit):
        """
        Event risen when enemy unit entered vision.
        If unit has not been previously been seen it is noted to dictionary and fitness value for countering unit is
        risen.
        If unit has entered vision and is near our structure raise a flag to boost values of army units.

        :param unit: Unit object that represents object in game
        """
        if unit.tag not in self.found_enemy_units.keys():
            self.found_enemy_units[unit.tag] = unit
            self.__enemy_unit_spotted(unit)
        if unit.can_attack and self.not_worker(unit):
            if not self.under_attack:
                if self.enemy_units.in_distance_of_group(self.structures, 10):
                    self.under_attack = True
                    logger.info("Enemy is attacking")

    async def on_enemy_unit_left_vision(self, unit_tag: int):
        """
        Event risen when enemy unit left vision.
        This Method calls another to check if there are still units in range of bot's structures.

        :param unit_tag: Unit object that represents object in game
        """
        await self.on_enemy_attacking_base()

    def __enemy_unit_spotted(self, unit: Unit):
        """
        Method called when new enemy unit has been spotted. It call object containing fitness value to increase
        values for units and buildings that counters spotted unit

        :param unit: Unit object of spotted enemy unit
        """
        self.zerg_values.enemy_unit_spotted(unit.type_id)

    def enemy_unit_destroyed(self, unit: Unit):
        """
        Method called when known enemy unit has been destroyed. It call object containing fitness value to decrease
        values for units and buildings that counters destroyed unit

        :param unit: Unit object of destroyed enemy unit
        """
        self.zerg_values.enemy_unit_destroyed(unit.type_id)

    async def on_unit_destroyed(self, unit_tag: int):
        """
        Event risen when any unit object has been destroyed.
        If enemy unit was destroyed it call for decreases in fitness value.
        If scouting unit was destroyed it schedules creation of new unit.
        If currently occupied geyser had it structure destroyed unlock this geyser position for building
        If main base was destroyed find new base, in case of not having other bases try to create new.

        :param unit_tag: tag of a unit that was destroyed.
        """
        if unit_tag in self.found_enemy_units.keys():
            unit = self.found_enemy_units.pop(unit_tag, None)
            self.enemy_unit_destroyed(unit)
            await self.on_enemy_attacking_base()
        elif unit_tag == self.scouting_overlord_tag:
            self.path.insert(0, ZergUnits.Units.OVERLORD)
        elif unit_tag in self.vespene_geyser:
            self.vespene_geyser.remove(unit_tag)
        elif unit_tag == self.main_base.tag:
            if self.townhalls.amount:
                self.main_base = self.townhalls.first
            else:
                self.path.insert(0, ZergBuildings.Buildings.HATCHERY)

    async def on_enemy_attacking_base(self):
        """
        Method that is called when enemy unit is destroyed or left vision. It resets flag that increase value of army
        units if there aren't any enemy units nearby bot's structures
        """
        if self.under_attack:
            if self.enemy_units.in_distance_of_group(self.structures, 10).empty:
                self.under_attack = False
                logger.info("Enemy destroyed or retreated")

    async def on_unit_created(self, unit: Unit):
        """
        Event risen when  unit has been created.
        If unit can be used to attack it send move command to gathering point.
        It decrement queen and overlord count that has been under construction

        :param unit: Unit object of unit that has been created
        """
        if self.is_eligible_to_attack(unit):
            unit.move(self.gathering_point)
        if unit.type_id == ZergUnits.Units.QUEEN.type_id:
            if self.queens_in_queue > 0:
                self.queens_in_queue -= 1
        if unit.type_id == ZergUnits.Units.LOCUST.type_id:
            if self.group_attacking:
                self.group_attacking.append(unit)
        if unit.type_id == ZergUnits.Units.OVERLORD.type_id:
            if self.overlords_in_building >0:
                self.overlords_in_building -= 1

    def get_attacking_group(self) -> Units:
        """
        Returns units that can attack enemy

        :return: Units object with units that can attack enemy base
        """
        return self.units.filter(self.is_eligible_to_attack)

    def near_supply_cap(self):
        """
        Method that check if army limit is near limit of 200.

        :return: Boolean that states if army limit is near supply cap of 200. 
        """
        if self.supply_cap >= 198 and self.supply_left <= 10:
            if self.units.filter(lambda x: x.type_id == UnitTypeId.EGG).empty:
                return True
        return False

    async def on_unit_took_damage(self, unit: Unit, amount_damage_taken: float):
        """
        Event risen when  unit took damage.
        If unit is bot's unit and it's not assigned to attack enemy base it gather's unit on point of map on which 
        unit took damage.
        During fights uses abilities of swarm host , infestors and vipers.

        :param unit: Unit object of unit that took damage
        :param amount_damage_taken: value of damage taken
        """
        await self.manage_defence(unit)
        try:
            if unit.is_mine and unit.tag != self.scouting_overlord_tag:
                await self.use_swarm_host_ability()
                await self.use_infestors_ability()
                await self.use_vipers_ability()
        except AssertionError:
            pass

    async def manage_defence(self, unit: Unit):
        """
        Sends units to defend position of friendly unit.

        :param unit: Unit object 
        """
        if unit.is_mine and unit.tag != self.scouting_overlord_tag:
            if unit not in self.group_attacking:
                for defence_unit in self.units.filter(lambda x: x not in self.group_attacking).filter(
                        lambda x: self.is_eligible_to_defend(x)):
                    defence_unit.attack(unit.position)

    async def use_vipers_ability(self):
        """
        Uses viper ability to cast parasatic bomb if viper has enough energy during a fight.
        """
        vipers = self.units.filter(lambda x: x.type_id == ZergUnits.Units.VIPER.type_id)
        if vipers:
            for viper in vipers:
                if viper.energy >= 125:
                    closest_enemy = self.enemy_units.filter(lambda x: x.is_flying).closest_to(viper.position)
                    viper(AbilityId.PARASITICBOMB_PARASITICBOMB, closest_enemy)

    async def use_infestors_ability(self):
        """
         Uses infestor ability to cast fungal growth if infestor has enough energy during a fight.
         """
        infestors = self.units.filter(lambda x: x.type_id == ZergUnits.Units.INFESTOR.type_id)
        if infestors:
            for infestor in infestors:
                if infestor.energy >= 75:
                    closest_enemy = self.enemy_units.closest_to(infestor.position)
                    infestor(AbilityId.FUNGALGROWTH_FUNGALGROWTH, closest_enemy.position)

    async def use_swarm_host_ability(self):
        """
        Uses swarm host ability to spawn locusts  if swarm host has cooldown ready.
        """
        swarm_hosts = self.units.filter(lambda x: x.type_id == ZergUnits.Units.SWARM_HOST.type_id)
        if swarm_hosts:
            for host in swarm_hosts:
                host(AbilityId.EFFECT_SPAWNLOCUSTS, host.position)

    def get_expansion_list_in_order(self):
        """
        Return list of expansions in order from closest to furthest from main base
        """
        return sorted(self.expansion_locations_list, key=lambda expansion: expansion.distance_to(self.main_base))

    async def create_morphing_unit(self, unit_from_morph, unit_to_morph):
        """
        Method responsible for morphing unit from one to another

        :param unit_from_morph: ZergUnits.Units enum that describes which unit should morph
        :param unit_to_morph: ZergUnits.Units enum that describes to which unit should be morphed to
        :return: Boolean value that states if morph operation was successful
        """
        pre_morphed = self.units.filter(lambda x: x.type_id == unit_from_morph.type_id)
        if pre_morphed:
            if pre_morphed.first.train(unit_to_morph.type_id) != 0:
                return True
            return False
        else:
            return True

    def not_worker(self, unit: Unit):
        """
        Returns if given unit is not a worker

        :param unit: Unit object ot check
        :return: Boolean value if unit is not a worker.
        """
        if unit.type_id != ZergUnits.Units.DRONE and unit.type_id != ProtossUnits.Units.PROBE.type_id and \
                unit.type_id != TerranUnits.Units.SCV.type_id:
            return True
        return False

    def is_eligible_to_defend(self, unit):
        """
        Check whether unit is able to defend base

        :param unit: Unit object to check
        :return: Boolean value if unit can defend base
        """
        if unit.type_id != ZergUnits.Units.DRONE.type_id and \
                unit.type_id != ZergUnits.Units.OVERLORD.type_id:
            return True
        return False

    def is_eligible_to_attack(self, unit: Unit):
        """
        Check whether unit is able to attack enemy base

        :param unit: Unit object to check
        :return: Boolean value if unit can attack enemy base
        """
        if unit.type_id != ZergUnits.Units.DRONE.type_id and \
                unit.type_id != ZergUnits.Units.OVERLORD.type_id and \
                unit.type_id != ZergUnits.Units.QUEEN.type_id:
            return True
        return False

    def research(self, upgrade_type: UpgradeId) -> bool:
        """
        Overriden Method to research upgrades. Allows for queueing researches.
        For detailed information read research docs from BotAI.py

        :param upgrade_type: Upgrade that should be researched
        """
        assert (
                upgrade_type in UPGRADE_RESEARCHED_FROM
        ), f"Could not find upgrade {upgrade_type} in 'research from'-dictionary"

        # Not affordable
        if not self.can_afford(upgrade_type):
            return False

        research_structure_types: UnitTypeId = UPGRADE_RESEARCHED_FROM[upgrade_type]
        required_tech_building: Optional[UnitTypeId] = RESEARCH_INFO[research_structure_types][upgrade_type].get(
            "required_building", None
        )

        requirement_met = (
                required_tech_building is None or self.structure_type_build_progress(required_tech_building) == 1
        )
        if not requirement_met:
            return False

        # All upgrades right now that can be researched in spire and hatch can also be researched in their morphs
        equiv_structures = {
            UnitTypeId.SPIRE: {UnitTypeId.SPIRE, UnitTypeId.GREATERSPIRE},
            UnitTypeId.GREATERSPIRE: {UnitTypeId.SPIRE, UnitTypeId.GREATERSPIRE},
            UnitTypeId.HATCHERY: {UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE},
            UnitTypeId.LAIR: {UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE},
            UnitTypeId.HIVE: {UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE},
        }
        # Convert to a set, or equivalent structures are chosen
        # Overlord speed upgrade can be researched from hatchery, lair or hive
        research_structure_types: Set[UnitTypeId] = equiv_structures.get(
            research_structure_types, {research_structure_types}
        )

        structure: Unit
        for structure in self.structures:
            if (
                    # Structure can research this upgrade
                    structure.type_id in research_structure_types
                    # If structure hasn't received an action/order this frame
                    and structure.tag not in self.unit_tags_received_action
            ):
                # Can_afford check was already done earlier in this function
                successful_action: bool = self.do(
                    structure.research(upgrade_type, queue=True), subtract_cost=True, ignore_warning=True
                )
                return successful_action
        return False

    async def pop_created_item(self):
        """
        Delete first item from the solution list
        """
        self.path.pop(0)

    async def get_closest_mineral_worker(self, position) -> Union[Unit, None]:
        """
        Find closest mineral workers to given position

        :param position: Position which should be checked
        :return: Returns mineral worker that is closest to given position or None
        """
        mineral_workers = self.workers.filter(
            lambda unit: unit.order_target in self.mineral_field.tags
                         or (unit.is_carrying_minerals and unit.order_target in self.townhalls.tags))
        if mineral_workers:
            return mineral_workers.closest_to(position)
        return None

    async def get_workers_gathering_vespene(self):
        """
        Method to return count of workers currently gathering vespene

        :return: int value which is count of workers currently gathering vespene
        """
        extractors = self.structures.filter(lambda x: x.type_id == ZergBuildings.Buildings.EXTRACTOR.type_id).ready
        return sum(map(lambda x: x.assigned_harvesters, extractors))

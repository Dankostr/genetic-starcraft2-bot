from enum import Enum

from sc2.ids.unit_typeid import UnitTypeId


class ZergBuildings(object):
    class Buildings(Enum):

        @property
        def type_id(self):
            return self.value

        HATCHERY = UnitTypeId.HATCHERY
        SPINE_CRAWLER = UnitTypeId.SPINECRAWLER
        SPORE_CRAWLER = UnitTypeId.SPORECRAWLER
        EXTRACTOR = UnitTypeId.EXTRACTOR
        SPAWNING_POOL = UnitTypeId.SPAWNINGPOOL
        EVOLUTION_CHAMBER = UnitTypeId.EVOLUTIONCHAMBER
        ROACH_WARREN = UnitTypeId.ROACHWARREN
        BANELING_NEST = UnitTypeId.BANELINGNEST
        # CREEP_TUMOR = UnitTypeId.CREEPTUMOR
        LAIR = UnitTypeId.LAIR
        HYDRALISK_DEN = UnitTypeId.HYDRALISKDEN
        LURKER_DEN = UnitTypeId.LURKERDENMP
        INFESTATION_PIT = UnitTypeId.INFESTATIONPIT
        SPIRE = UnitTypeId.SPIRE
        NYDUS_NETWORK = UnitTypeId.NYDUSNETWORK
        HIVE = UnitTypeId.HIVE
        GREATER_SPIRE = UnitTypeId.GREATERSPIRE
        ULTRALISK_CAVERN = UnitTypeId.ULTRALISKCAVERN

    building_list = list(Buildings)
    evolution_building_set = {Buildings.GREATER_SPIRE,Buildings.LAIR,Buildings.HIVE}

    dependencies = {Buildings.HATCHERY: set(),
                    Buildings.SPINE_CRAWLER: {Buildings.SPAWNING_POOL},
                    Buildings.SPORE_CRAWLER: {Buildings.SPAWNING_POOL},
                    Buildings.EXTRACTOR: set(),
                    Buildings.SPAWNING_POOL: {Buildings.HATCHERY},
                    Buildings.EVOLUTION_CHAMBER: {Buildings.HATCHERY},
                    Buildings.ROACH_WARREN: {Buildings.SPAWNING_POOL},
                    Buildings.BANELING_NEST: {Buildings.SPAWNING_POOL, Buildings.EXTRACTOR},
                    Buildings.LAIR: {Buildings.HATCHERY, Buildings.SPAWNING_POOL,
                                     Buildings.EXTRACTOR},
                    # Buildings.CREEP_TUMOR: None,
                    Buildings.HYDRALISK_DEN: {Buildings.LAIR, Buildings.EXTRACTOR},
                    Buildings.LURKER_DEN: {Buildings.HYDRALISK_DEN, Buildings.EXTRACTOR},
                    Buildings.INFESTATION_PIT: {Buildings.LAIR, Buildings.EXTRACTOR},
                    Buildings.SPIRE: {Buildings.LAIR, Buildings.EXTRACTOR},
                    Buildings.NYDUS_NETWORK: {Buildings.LAIR, Buildings.EXTRACTOR},
                    Buildings.HIVE: {Buildings.LAIR, Buildings.INFESTATION_PIT,
                                     Buildings.EXTRACTOR},
                    Buildings.GREATER_SPIRE: {Buildings.SPIRE, Buildings.HIVE, Buildings.EXTRACTOR},
                    Buildings.ULTRALISK_CAVERN: {Buildings.HIVE, Buildings.EXTRACTOR}
                    }

    time = {
        Buildings.HATCHERY: 71,
        Buildings.SPINE_CRAWLER: 36,
        Buildings.SPORE_CRAWLER: 21,
        Buildings.EXTRACTOR: 21,
        Buildings.SPAWNING_POOL: 46,
        Buildings.EVOLUTION_CHAMBER: 25,
        Buildings.ROACH_WARREN: 39,
        Buildings.BANELING_NEST: 43,
        Buildings.LAIR: 57,
        # Buildings.CREEP_TUMOR: None,
        Buildings.HYDRALISK_DEN: 29,
        Buildings.LURKER_DEN: 57,
        Buildings.INFESTATION_PIT: 36,
        Buildings.SPIRE: 71,
        Buildings.NYDUS_NETWORK: 36,
        Buildings.HIVE: 71,
        Buildings.GREATER_SPIRE: 71,
        Buildings.ULTRALISK_CAVERN: 46
    }

    resources = {
        Buildings.HATCHERY: (300, 0),
        Buildings.SPINE_CRAWLER: (100, 0),
        Buildings.SPORE_CRAWLER: (75, 0),
        Buildings.EXTRACTOR: (25, 0),
        Buildings.SPAWNING_POOL: (200, 0),
        Buildings.EVOLUTION_CHAMBER: (75, 0),
        Buildings.ROACH_WARREN: (150, 0),
        Buildings.BANELING_NEST: (100, 50),
        Buildings.LAIR: (150, 100),
        # Buildings.CREEP_TUMOR: None,
        Buildings.HYDRALISK_DEN: (100, 100),
        Buildings.LURKER_DEN: (100, 150),
        Buildings.INFESTATION_PIT: (100, 100),
        Buildings.SPIRE: (200, 200),
        Buildings.NYDUS_NETWORK: (150, 150),
        Buildings.HIVE: (200, 150),
        Buildings.GREATER_SPIRE: (100, 150),
        Buildings.ULTRALISK_CAVERN: (150, 200)
    }

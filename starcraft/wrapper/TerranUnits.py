from enum import Enum

from sc2 import UnitTypeId

from starcraft.wrapper.ZergUnits import ZergUnits


class TerranUnits(object):
    class Units(Enum):

        @property
        def type_id(self):
            return self.value

        SCV = UnitTypeId.SCV
        MULE = UnitTypeId.MULE
        MARINE = UnitTypeId.MARINE
        MARAUDER = UnitTypeId.MARAUDER
        REAPER = UnitTypeId.REAPER
        GHOST = UnitTypeId.GHOST
        HELLION = UnitTypeId.HELLION
        SIEGE_TANK = UnitTypeId.SIEGETANK
        CYCLONE = UnitTypeId.CYCLONE
        WIDOW_MINE = UnitTypeId.WIDOWMINE
        THOR = UnitTypeId.THOR
        AUTO_TURRET = UnitTypeId.AUTOTURRET
        VIKING = UnitTypeId.VIKING
        MEDIVAC = UnitTypeId.MEDIVAC
        LIBERATOR = UnitTypeId.LIBERATOR
        RAVEN = UnitTypeId.RAVEN
        BANSHEE = UnitTypeId.BANSHEE
        BATTLECRUISER = UnitTypeId.BATTLECRUISER

    units_list = list(Units)

    IS_COUNTERED = {
        Units.SCV: set(),
        Units.MULE: set(),
        Units.MARINE: {ZergUnits.Units.BANELING, ZergUnits.Units.ROACH, ZergUnits.Units.ULTRALISK,
                       ZergUnits.Units.BROOD_LORD},
        Units.MARAUDER: {ZergUnits.Units.ZERGLING, ZergUnits.Units.HYRDRALISK,
                         ZergUnits.Units.MUTALISK, ZergUnits.Units.BROOD_LORD},
        Units.REAPER: {ZergUnits.Units.ROACH},
        Units.GHOST: {ZergUnits.Units.ZERGLING, ZergUnits.Units.ROACH, ZergUnits.Units.ULTRALISK},
        Units.HELLION: {ZergUnits.Units.ROACH, ZergUnits.Units.MUTALISK},
        Units.SIEGE_TANK: {ZergUnits.Units.MUTALISK, ZergUnits.Units.BROOD_LORD},
        Units.CYCLONE: {ZergUnits.Units.ZERGLING},
        Units.WIDOW_MINE: {ZergUnits.Units.RAVAGER},
        Units.THOR: {ZergUnits.Units.ZERGLING, ZergUnits.Units.HYRDRALISK,
                     ZergUnits.Units.BROOD_LORD},
        Units.AUTO_TURRET: set(),
        Units.VIKING: {ZergUnits.Units.HYRDRALISK},
        Units.MEDIVAC: {ZergUnits.Units.MUTALISK, ZergUnits.Units.HYRDRALISK},
        Units.LIBERATOR: {ZergUnits.Units.CORRUPTOR},
        Units.RAVEN: {ZergUnits.Units.CORRUPTOR},
        Units.BANSHEE: {ZergUnits.Units.HYRDRALISK, ZergUnits.Units.MUTALISK,
                        ZergUnits.Units.CORRUPTOR},
        Units.BATTLECRUISER: {ZergUnits.Units.CORRUPTOR},
    }

    COUNTERS = {
        Units.SCV: set(),
        Units.MULE: set(),
        Units.MARINE: {ZergUnits.Units.CORRUPTOR},
        Units.MARAUDER: {ZergUnits.Units.ROACH},
        Units.REAPER: set(),
        Units.GHOST: {ZergUnits.Units.INFESTOR},
        Units.HELLION: {ZergUnits.Units.ZERGLING, ZergUnits.Units.MUTALISK},
        Units.SIEGE_TANK: {ZergUnits.Units.ROACH, ZergUnits.Units.MUTALISK,
                           ZergUnits.Units.INFESTOR,
                           ZergUnits.Units.BANELING},
        Units.CYCLONE: {ZergUnits.Units.ROACH},
        Units.WIDOW_MINE: {ZergUnits.Units.MUTALISK, ZergUnits.Units.ZERGLING,
                           ZergUnits.Units.BANELING},
        Units.THOR: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR,
                     ZergUnits.Units.ULTRALISK},
        Units.AUTO_TURRET: set(),
        Units.VIKING: {ZergUnits.Units.BROOD_LORD},
        Units.MEDIVAC: set(),
        Units.LIBERATOR: {ZergUnits.Units.MUTALISK},
        Units.RAVEN: {ZergUnits.Units.ROACH},
        Units.BANSHEE: {ZergUnits.Units.ULTRALISK},
        Units.BATTLECRUISER: {ZergUnits.Units.MUTALISK},
    }

from starcraft.wrapper.ProtossUnits import ProtossUnits
from starcraft.wrapper.TerranUnits import TerranUnits
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits
from starcraft.wrapper.ZergUpgrades import ZergUpgrades
from utils.singleton import Singleton


class SC2Helper(object, metaclass=Singleton):

    def __init__(self):
        self.timers = SC2Helper.merge_dicts(SC2Helper.merge_dicts(ZergBuildings.time, ZergUnits.time),ZergUpgrades.time)
        self.resources = SC2Helper.merge_dicts(SC2Helper.merge_dicts(ZergBuildings.resources, ZergUnits.resources),
                                               ZergUpgrades.resources)
        self.dependencies = SC2Helper.merge_dicts(SC2Helper.merge_dicts(ZergBuildings.dependencies,
                                                  ZergUnits.dependencies),ZergUpgrades.dependencies)
        self.army_slots = ZergUnits.supply
        self.unit_counters = SC2Helper.merge_dicts(SC2Helper.merge_dicts(ZergUnits.COUNTERS, TerranUnits.COUNTERS),
                                                   ProtossUnits.COUNTERS)
        self.unit_is_countered = SC2Helper.merge_dicts(SC2Helper.merge_dicts(ZergUnits.IS_COUNTERED,
                                                                             TerranUnits.IS_COUNTERED, ),
                                                       ProtossUnits.IS_COUNTERED)
        self.affected_units_after_upgrade = ZergUpgrades.affects

    def get_timers(self) -> dict:
        return self.timers

    def get_resources_costs(self, item) -> dict:
        return self.resources[item]

    def get_dependencies(self) -> dict:
        return self.dependencies

    def get_unit_dependencies(self, unit) -> set:
        return self.dependencies[unit]

    def get_unit_time_value(self, item) -> int:
        return self.timers[item]

    def get_unit_army_slots(self, item) -> int:
        return self.army_slots[item]

    def get_units_that_counter_given_unit(self, item) -> set:
        return self.unit_is_countered[item]

    def get_units_that_are_countered_by_given_unit(self, item) -> set:
        return self.unit_counters[item]

    def get_affected_units(self,upgrade) -> set:
        return self.affected_units_after_upgrade[upgrade]

    @staticmethod
    def merge_dicts(dict1: dict, dict2: dict):
        d = dict1.copy()
        d.update(dict2)
        return d

    @staticmethod
    def get_army_unit_by_type_id(unit_typeid):
        for unit in ZergUnits.units_list:
            if unit.type_id == unit_typeid:
                return unit
        for unit in ProtossUnits.units_list:
            if unit.type_id == unit_typeid:
                return unit
        for unit in TerranUnits.units_list:
            if unit.type_id == unit_typeid:
                return unit
        return None

    @staticmethod
    def get_building_by_type_id(unit_typeid):
        for unit in ZergBuildings.building_list:
            if unit.type_id == unit_typeid:
                return unit

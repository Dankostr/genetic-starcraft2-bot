from enum import Enum

from sc2.ids.unit_typeid import UnitTypeId

from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class ProtossUnits(object):
    class Units(Enum):

        @property
        def type_id(self):
            return self.value

        PROBE = UnitTypeId.PROBE
        ZEALOT = UnitTypeId.ZEALOT
        STALKER = UnitTypeId.STALKER
        SENTRY = UnitTypeId.SENTRY
        ADEPT = UnitTypeId.ADEPT
        HIGH_TEMPLAR = UnitTypeId.HIGHTEMPLAR
        DARK_TEMPLAR = UnitTypeId.DARKTEMPLAR
        IMMORTAL = UnitTypeId.IMMORTAL
        COLOSSUS = UnitTypeId.COLOSSUS
        DISRUPTOR = UnitTypeId.DISRUPTOR
        ARCHON = UnitTypeId.ARCHON
        OBSERVER = UnitTypeId.OBSERVER
        WARP_PRISM = UnitTypeId.WARPPRISM
        PHOENIX = UnitTypeId.PHOENIX
        VOID_RAY = UnitTypeId.VOIDRAY
        ORACLE = UnitTypeId.ORACLE
        CARRIER = UnitTypeId.CARRIER
        TEMPEST = UnitTypeId.TEMPEST
        MOTHERSHIP = UnitTypeId.MOTHERSHIP
        MOTHERSHIP_CORE = UnitTypeId.MOTHERSHIPCORE

    units_list = list(Units)

    IS_COUNTERED = {
        Units.PROBE: set(),
        Units.ZEALOT: {ZergUnits.Units.ROACH, ZergUnits.Units.MUTALISK, ZergUnits.Units.BROOD_LORD},
        Units.STALKER: {ZergUnits.Units.ZERGLING, ZergUnits.Units.ROACH,
                        ZergUnits.Units.HYRDRALISK},
        Units.SENTRY: {ZergUnits.Units.HYRDRALISK, ZergUnits.Units.BROOD_LORD,
                       ZergUnits.Units.ULTRALISK},
        Units.ADEPT: {ZergUnits.Units.ROACH},
        Units.HIGH_TEMPLAR: {ZergUnits.Units.ROACH, ZergUnits.Units.ULTRALISK},
        Units.DARK_TEMPLAR: {ZergUnits.Units.MUTALISK, ZergUnits.Units.BROOD_LORD},
        Units.IMMORTAL: {ZergUnits.Units.ZERGLING, ZergUnits.Units.HYRDRALISK},
        Units.COLOSSUS: {ZergUnits.Units.CORRUPTOR},
        Units.DISRUPTOR: {ZergUnits.Units.ULTRALISK},
        Units.ARCHON: {ZergUnits.Units.ULTRALISK},
        Units.OBSERVER: {ZergBuildings.Buildings.SPORE_CRAWLER},
        Units.WARP_PRISM: {ZergUnits.Units.HYRDRALISK, ZergUnits.Units.MUTALISK},
        Units.PHOENIX: {ZergUnits.Units.CORRUPTOR, ZergUnits.Units.HYRDRALISK},
        Units.VOID_RAY: {ZergUnits.Units.MUTALISK, ZergUnits.Units.HYRDRALISK},
        Units.ORACLE: {ZergUnits.Units.MUTALISK},
        Units.CARRIER: {ZergUnits.Units.CORRUPTOR},
        Units.TEMPEST: {ZergUnits.Units.CORRUPTOR},
        Units.MOTHERSHIP: {ZergUnits.Units.CORRUPTOR},
        Units.MOTHERSHIP_CORE: {ZergUnits.Units.CORRUPTOR},
    }

    COUNTERS = {
        Units.PROBE: set(),
        Units.ZEALOT: {ZergUnits.Units.ZERGLING},
        Units.STALKER: {ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD},
        Units.SENTRY: {ZergUnits.Units.HYRDRALISK, ZergUnits.Units.MUTALISK,
                       ZergUnits.Units.CORRUPTOR},
        Units.ADEPT: {ZergUnits.Units.ZERGLING},
        Units.HIGH_TEMPLAR: {ZergUnits.Units.HYRDRALISK},
        Units.DARK_TEMPLAR: set(),
        Units.IMMORTAL: {ZergUnits.Units.ROACH, ZergUnits.Units.INFESTOR,
                         ZergUnits.Units.ULTRALISK},
        Units.COLOSSUS: {ZergUnits.Units.ZERGLING, ZergUnits.Units.HYRDRALISK,
                         ZergUnits.Units.INFESTOR, ZergUnits.Units.BANELING},
        Units.DISRUPTOR: {ZergUnits.Units.HYRDRALISK},
        Units.ARCHON: {ZergUnits.Units.MUTALISK},
        Units.OBSERVER: {ZergUnits.Units.ROACH},
        Units.WARP_PRISM: set(),
        Units.PHOENIX: {ZergUnits.Units.MUTALISK,ZergUnits.Units.ROACH,ZergUnits.Units.ZERGLING},
        Units.VOID_RAY: {ZergUnits.Units.ROACH, ZergUnits.Units.ULTRALISK,
                         ZergUnits.Units.BROOD_LORD},
        Units.ORACLE: set(),
        Units.CARRIER: {ZergUnits.Units.MUTALISK},
        Units.TEMPEST: {ZergUnits.Units.BROOD_LORD},
        Units.MOTHERSHIP: set(),
        Units.MOTHERSHIP_CORE: set(),
    }

from enum import Enum

from sc2.ids.upgrade_id import UpgradeId

from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.ZergUnits import ZergUnits


class ZergUpgrades(object):
    class Upgrades(Enum):

        @property
        def type_id(self):
            return self.value

        # Ataack upgrades
        MELEE_ATTACK_1 = UpgradeId.ZERGMELEEWEAPONSLEVEL1
        MELEE_ATTACK_2 = UpgradeId.ZERGMELEEWEAPONSLEVEL2
        MELEE_ATTACK_3 = UpgradeId.ZERGMELEEWEAPONSLEVEL3
        MISSILE_ATTACK_1 = UpgradeId.ZERGMISSILEWEAPONSLEVEL1
        MISSILE_ATTACK_2 = UpgradeId.ZERGMISSILEWEAPONSLEVEL2
        MISSILE_ATTACK_3 = UpgradeId.ZERGMISSILEWEAPONSLEVEL3
        FLYER_ATTACKS_1 = UpgradeId.ZERGFLYERWEAPONSLEVEL1
        FLYER_ATTACKS_2 = UpgradeId.ZERGFLYERWEAPONSLEVEL2
        FLYER_ATTACKS_3 = UpgradeId.ZERGFLYERWEAPONSLEVEL3
        # Armor upgrades
        GROUND_CARAPACE_1 = UpgradeId.ZERGGROUNDARMORSLEVEL1
        GROUND_CARAPACE_2 = UpgradeId.ZERGGROUNDARMORSLEVEL2
        GROUND_CARAPACE_3 = UpgradeId.ZERGGROUNDARMORSLEVEL3
        FLYER_CARAPACE_1 = UpgradeId.ZERGFLYERARMORSLEVEL1
        FLYER_CARAPACE_2 = UpgradeId.ZERGFLYERARMORSLEVEL2
        FLYER_CARAPACE_3 = UpgradeId.ZERGFLYERARMORSLEVEL3
        CHITINOUS_PLATING = UpgradeId.CHITINOUSPLATING
        # Speed upgrades
        # ADAPTIVE_TALONS = NOT FOUND IN API
        ANABOLIC_SYNTHESIS = UpgradeId.ANABOLICSYNTHESIS
        CENTRIFUGAL_HOOKS = UpgradeId.CENTRIFICALHOOKS
        GLIAL_RECONSTITUTION = UpgradeId.GLIALRECONSTITUTION
        METABOLIC_BOOST = UpgradeId.ZERGLINGMOVEMENTSPEED
        PNEUMATIZED_CARAPACE = UpgradeId.OVERLORDSPEED
        MUSCULAR_AUGMENTS = UpgradeId.EVOLVEMUSCULARAUGMENTS
        # RANGE
        GROOVED_SPINES = UpgradeId.EVOLVEGROOVEDSPINES
        SEISMIC_SPINES = UpgradeId.LURKERRANGE

    upgrades_list = list(Upgrades)

    dependencies = {
        Upgrades.MELEE_ATTACK_1: {ZergBuildings.Buildings.EVOLUTION_CHAMBER, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MELEE_ATTACK_2: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                  Upgrades.MELEE_ATTACK_1, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MELEE_ATTACK_3: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                  Upgrades.MELEE_ATTACK_2, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MISSILE_ATTACK_1: {ZergBuildings.Buildings.EVOLUTION_CHAMBER, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MISSILE_ATTACK_2: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                    Upgrades.MISSILE_ATTACK_1, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MISSILE_ATTACK_3: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                    Upgrades.MISSILE_ATTACK_2, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_ATTACKS_1: {ZergBuildings.Buildings.SPIRE, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_ATTACKS_2: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.SPIRE,
                                   Upgrades.FLYER_ATTACKS_1, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_ATTACKS_3: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.SPIRE,
                                   Upgrades.FLYER_ATTACKS_2, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.GROUND_CARAPACE_1: {ZergBuildings.Buildings.EVOLUTION_CHAMBER, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.GROUND_CARAPACE_2: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                     Upgrades.GROUND_CARAPACE_1, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.GROUND_CARAPACE_3: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.EVOLUTION_CHAMBER,
                                     Upgrades.GROUND_CARAPACE_2, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_CARAPACE_1: {ZergBuildings.Buildings.SPIRE, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_CARAPACE_2: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.SPIRE,
                                    Upgrades.FLYER_CARAPACE_1, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.FLYER_CARAPACE_3: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.SPIRE,
                                    Upgrades.FLYER_CARAPACE_2, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.CHITINOUS_PLATING: {ZergBuildings.Buildings.ULTRALISK_CAVERN, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.ANABOLIC_SYNTHESIS: {ZergBuildings.Buildings.ULTRALISK_CAVERN, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.CENTRIFUGAL_HOOKS: {ZergBuildings.Buildings.BANELING_NEST, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.GLIAL_RECONSTITUTION: {ZergBuildings.Buildings.ROACH_WARREN, ZergBuildings.Buildings.LAIR,
                                        ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.METABOLIC_BOOST: {ZergBuildings.Buildings.SPAWNING_POOL, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.PNEUMATIZED_CARAPACE: {ZergBuildings.Buildings.HATCHERY, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.MUSCULAR_AUGMENTS: {ZergBuildings.Buildings.LAIR, ZergBuildings.Buildings.HYDRALISK_DEN,
                                     ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.GROOVED_SPINES: {ZergBuildings.Buildings.HYDRALISK_DEN, ZergBuildings.Buildings.EXTRACTOR},
        Upgrades.SEISMIC_SPINES: {ZergBuildings.Buildings.LURKER_DEN, ZergBuildings.Buildings.HIVE,
                                  ZergBuildings.Buildings.EXTRACTOR}
    }

    time = {
        Upgrades.MELEE_ATTACK_1: 114,
        Upgrades.MELEE_ATTACK_2: 136,
        Upgrades.MELEE_ATTACK_3: 157,
        Upgrades.MISSILE_ATTACK_1: 114,
        Upgrades.MISSILE_ATTACK_2: 136,
        Upgrades.MISSILE_ATTACK_3: 157,
        Upgrades.FLYER_ATTACKS_1: 114,
        Upgrades.FLYER_ATTACKS_2: 136,
        Upgrades.FLYER_ATTACKS_3: 157,
        Upgrades.GROUND_CARAPACE_1: 114,
        Upgrades.GROUND_CARAPACE_2: 136,
        Upgrades.GROUND_CARAPACE_3: 157,
        Upgrades.FLYER_CARAPACE_1: 114,
        Upgrades.FLYER_CARAPACE_2: 136,
        Upgrades.FLYER_CARAPACE_3: 157,
        Upgrades.CHITINOUS_PLATING: 79,
        Upgrades.ANABOLIC_SYNTHESIS: 42.9,
        Upgrades.CENTRIFUGAL_HOOKS: 79,
        Upgrades.GLIAL_RECONSTITUTION: 79,
        Upgrades.METABOLIC_BOOST: 79,
        Upgrades.PNEUMATIZED_CARAPACE: 43,
        Upgrades.MUSCULAR_AUGMENTS: 71,
        Upgrades.GROOVED_SPINES: 71,
        Upgrades.SEISMIC_SPINES: 57,
    }

    resources = {
        Upgrades.MELEE_ATTACK_1: (100, 100),
        Upgrades.MELEE_ATTACK_2: (150, 150),
        Upgrades.MELEE_ATTACK_3: (200, 200),
        Upgrades.MISSILE_ATTACK_1: (100, 100),
        Upgrades.MISSILE_ATTACK_2: (150, 150),
        Upgrades.MISSILE_ATTACK_3: (200, 200),
        Upgrades.FLYER_ATTACKS_1: (100, 100),
        Upgrades.FLYER_ATTACKS_2: (175, 175),
        Upgrades.FLYER_ATTACKS_3: (250, 250),
        Upgrades.GROUND_CARAPACE_1: (150, 150),
        Upgrades.GROUND_CARAPACE_2: (225, 255),
        Upgrades.GROUND_CARAPACE_3: (300, 300),
        Upgrades.FLYER_CARAPACE_1: (150, 150),
        Upgrades.FLYER_CARAPACE_2: (225, 225),
        Upgrades.FLYER_CARAPACE_3: (300, 300),
        Upgrades.CHITINOUS_PLATING: (150, 150),
        Upgrades.ANABOLIC_SYNTHESIS: (150, 150),
        Upgrades.CENTRIFUGAL_HOOKS: (150, 150),
        Upgrades.GLIAL_RECONSTITUTION: (100, 100),
        Upgrades.METABOLIC_BOOST: (100, 100),
        Upgrades.PNEUMATIZED_CARAPACE: (100, 100),
        Upgrades.MUSCULAR_AUGMENTS: (100, 100),
        Upgrades.GROOVED_SPINES: (100, 100),
        Upgrades.SEISMIC_SPINES: (150, 150)
    }

    affects = {
        Upgrades.MELEE_ATTACK_1: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK},
        Upgrades.MELEE_ATTACK_2: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK},
        Upgrades.MELEE_ATTACK_3: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK},
        Upgrades.MISSILE_ATTACK_1: {ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                    ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                    ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.MISSILE_ATTACK_2: {ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                    ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                    ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.MISSILE_ATTACK_3: {ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                    ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                    ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.FLYER_ATTACKS_1: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                   ZergUnits.Units.VIPER},
        Upgrades.FLYER_ATTACKS_2: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                   ZergUnits.Units.VIPER},
        Upgrades.FLYER_ATTACKS_3: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                   ZergUnits.Units.VIPER},
        Upgrades.GROUND_CARAPACE_1: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK,
                                     ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                     ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                     ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.GROUND_CARAPACE_2: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK,
                                     ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                     ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                     ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.GROUND_CARAPACE_3: {ZergUnits.Units.ZERGLING, ZergUnits.Units.BANELING, ZergUnits.Units.ULTRALISK,
                                     ZergUnits.Units.ROACH, ZergUnits.Units.RAVAGER, ZergUnits.Units.HYRDRALISK,
                                     ZergUnits.Units.LURKER, ZergUnits.Units.INFESTOR, ZergUnits.Units.SWARM_HOST,
                                     ZergUnits.Units.ULTRALISK, ZergUnits.Units.LOCUST, ZergUnits.Units.CHANGELING},
        Upgrades.FLYER_CARAPACE_1: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                    ZergUnits.Units.VIPER},
        Upgrades.FLYER_CARAPACE_2: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                    ZergUnits.Units.VIPER},
        Upgrades.FLYER_CARAPACE_3: {ZergUnits.Units.MUTALISK, ZergUnits.Units.CORRUPTOR, ZergUnits.Units.BROOD_LORD,
                                    ZergUnits.Units.VIPER},
        Upgrades.CHITINOUS_PLATING: {ZergUnits.Units.ULTRALISK},
        Upgrades.ANABOLIC_SYNTHESIS: {ZergUnits.Units.ULTRALISK},
        Upgrades.CENTRIFUGAL_HOOKS: {ZergUnits.Units.BANELING},
        Upgrades.GLIAL_RECONSTITUTION: {ZergUnits.Units.ROACH},
        Upgrades.METABOLIC_BOOST: {ZergUnits.Units.ZERGLING},
        Upgrades.PNEUMATIZED_CARAPACE: {ZergUnits.Units.OVERLORD, ZergUnits.Units.OVERSEER},
        Upgrades.MUSCULAR_AUGMENTS: {ZergUnits.Units.HYRDRALISK},
        Upgrades.GROOVED_SPINES: {ZergUnits.Units.HYRDRALISK},
        Upgrades.SEISMIC_SPINES: {ZergUnits.Units.LURKER}
    }

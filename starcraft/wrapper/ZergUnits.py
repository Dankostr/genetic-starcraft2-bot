from enum import Enum

from sc2.ids.unit_typeid import UnitTypeId

from starcraft.wrapper.ZergBuildings import ZergBuildings


class ZergUnits(object):
    class Units(Enum):

        @property
        def type_id(self):
            return self.value

        DRONE = UnitTypeId.DRONE
        QUEEN = UnitTypeId.QUEEN
        ZERGLING = UnitTypeId.ZERGLING
        BANELING = UnitTypeId.BANELING
        ROACH = UnitTypeId.ROACH
        RAVAGER = UnitTypeId.RAVAGER
        HYRDRALISK = UnitTypeId.HYDRALISK
        LURKER = UnitTypeId.LURKERMP
        INFESTOR = UnitTypeId.INFESTOR
        SWARM_HOST = UnitTypeId.SWARMHOSTMP
        ULTRALISK = UnitTypeId.ULTRALISK
        LOCUST = UnitTypeId.LOCUSTMP
        CHANGELING = UnitTypeId.CHANGELING
        OVERLORD = UnitTypeId.OVERLORD
        OVERSEER = UnitTypeId.OVERSEER
        MUTALISK = UnitTypeId.MUTALISK
        CORRUPTOR = UnitTypeId.CORRUPTOR
        BROOD_LORD = UnitTypeId.BROODLORD
        VIPER = UnitTypeId.VIPER

    units_list = list(Units)

    dependencies = {
        Units.DRONE: set(),
        Units.QUEEN: {ZergBuildings.Buildings.SPAWNING_POOL},
        Units.ZERGLING: {ZergBuildings.Buildings.SPAWNING_POOL},
        Units.BANELING: {ZergBuildings.Buildings.BANELING_NEST, ZergBuildings.Buildings.EXTRACTOR},
        Units.ROACH: {ZergBuildings.Buildings.ROACH_WARREN, ZergBuildings.Buildings.EXTRACTOR},
        Units.RAVAGER: {ZergBuildings.Buildings.ROACH_WARREN, Units.ROACH,
                        ZergBuildings.Buildings.EXTRACTOR},
        Units.HYRDRALISK: {ZergBuildings.Buildings.HYDRALISK_DEN,
                           ZergBuildings.Buildings.EXTRACTOR},
        Units.LURKER: {ZergBuildings.Buildings.LURKER_DEN, Units.HYRDRALISK,
                       ZergBuildings.Buildings.EXTRACTOR},
        Units.INFESTOR: {ZergBuildings.Buildings.INFESTATION_PIT,
                         ZergBuildings.Buildings.EXTRACTOR},
        Units.SWARM_HOST: {ZergBuildings.Buildings.INFESTATION_PIT,
                           ZergBuildings.Buildings.EXTRACTOR},
        Units.ULTRALISK: {ZergBuildings.Buildings.ULTRALISK_CAVERN,
                          ZergBuildings.Buildings.EXTRACTOR},
        Units.LOCUST: {Units.SWARM_HOST},
        Units.CHANGELING: {Units.OVERSEER},
        Units.OVERLORD: set(),
        Units.OVERSEER: {ZergBuildings.Buildings.LAIR, Units.OVERLORD,
                         ZergBuildings.Buildings.EXTRACTOR},
        Units.MUTALISK: {ZergBuildings.Buildings.SPIRE, ZergBuildings.Buildings.EXTRACTOR},
        Units.CORRUPTOR: {ZergBuildings.Buildings.SPIRE, ZergBuildings.Buildings.EXTRACTOR},
        Units.BROOD_LORD: {ZergBuildings.Buildings.GREATER_SPIRE,
                           ZergBuildings.Buildings.EXTRACTOR},
        Units.VIPER: {ZergBuildings.Buildings.HIVE, ZergBuildings.Buildings.EXTRACTOR}
    }

    time = {
        Units.DRONE: 12,
        Units.QUEEN: 36,
        Units.ZERGLING: 17,
        Units.BANELING: 14,
        Units.ROACH: 19,
        Units.RAVAGER: 9,
        Units.HYRDRALISK: 24,
        Units.LURKER: 18,
        Units.INFESTOR: 36,
        Units.SWARM_HOST: 29,
        Units.ULTRALISK: 39,
        Units.LOCUST: 3.6,
        Units.CHANGELING: 0,
        Units.OVERLORD: 18,
        Units.OVERSEER: 12,
        Units.MUTALISK: 24,
        Units.CORRUPTOR: 29,
        Units.BROOD_LORD: 24,
        Units.VIPER: 29
    }

    supply = {
        Units.DRONE: 1,
        Units.QUEEN: 2,
        Units.ZERGLING: 1,
        Units.BANELING: 0.5,
        Units.ROACH: 2,
        Units.RAVAGER: 1,
        Units.HYRDRALISK: 2,
        Units.LURKER: 1,
        Units.INFESTOR: 2,
        Units.SWARM_HOST: 3,
        Units.ULTRALISK: 6,
        Units.LOCUST: 0,
        Units.CHANGELING: 0,
        Units.OVERLORD: 0,
        Units.OVERSEER: 0,
        Units.MUTALISK: 2,
        Units.CORRUPTOR: 2,
        Units.BROOD_LORD: 2,
        Units.VIPER: 3
    }

    resources = {
        Units.DRONE: (50, 0),
        Units.QUEEN: (150, 0),
        Units.ZERGLING: (25, 0),
        Units.BANELING: (25, 25),
        Units.ROACH: (75, 25),
        Units.RAVAGER: (25, 75),
        Units.HYRDRALISK: (100, 50),
        Units.LURKER: (50, 100),
        Units.INFESTOR: (100, 150),
        Units.SWARM_HOST: (100, 75),
        Units.ULTRALISK: (300, 200),
        Units.LOCUST: (0, 0),
        Units.CHANGELING: (0, 0),
        Units.OVERLORD: (100, 0),
        Units.OVERSEER: (50, 50),
        Units.MUTALISK: (100, 100),
        Units.CORRUPTOR: (150, 100),
        Units.BROOD_LORD: (150, 150),
        Units.VIPER: (100, 200)
    }

    IS_COUNTERED = {
        Units.DRONE: set(),
        Units.QUEEN: set(),
        Units.ZERGLING: {Units.ROACH, Units.ULTRALISK},
        Units.BANELING: {Units.MUTALISK, Units.ROACH, Units.ULTRALISK},
        Units.ROACH: {Units.MUTALISK, Units.BROOD_LORD},
        Units.RAVAGER: {Units.ULTRALISK},
        Units.HYRDRALISK: {Units.BROOD_LORD, Units.ROACH},
        Units.LURKER: {Units.ULTRALISK},
        Units.INFESTOR: {Units.BROOD_LORD},
        Units.SWARM_HOST: {Units.MUTALISK},
        Units.ULTRALISK: {Units.HYRDRALISK, Units.BROOD_LORD},
        Units.LOCUST: {Units.ULTRALISK},
        Units.CHANGELING: set(),
        Units.OVERLORD: set(),
        Units.OVERSEER: set(),
        Units.MUTALISK: {Units.HYRDRALISK},
        Units.CORRUPTOR: {Units.HYRDRALISK},
        Units.BROOD_LORD: {Units.CORRUPTOR},
        Units.VIPER: {Units.MUTALISK},
    }

    COUNTERS = {
        Units.DRONE: set(),
        Units.QUEEN: set(),
        Units.ZERGLING: set(),
        Units.BANELING: set(),
        Units.ROACH: {Units.ZERGLING, Units.BANELING, Units.HYRDRALISK},
        Units.RAVAGER: set(),
        Units.HYRDRALISK: {Units.ULTRALISK, Units.MUTALISK, Units.CORRUPTOR},
        Units.LURKER: set(),
        Units.INFESTOR: set(),
        Units.SWARM_HOST: set(),
        Units.ULTRALISK: {Units.ZERGLING, Units.BANELING, Units.RAVAGER, Units.LURKER,
                          Units.LOCUST},
        Units.LOCUST: set(),
        Units.CHANGELING: set(),
        Units.OVERLORD: set(),
        Units.OVERSEER: set(),
        Units.MUTALISK: {Units.BANELING, Units.ROACH, Units.SWARM_HOST, Units.VIPER},
        Units.CORRUPTOR: {Units.BROOD_LORD},
        Units.BROOD_LORD: {Units.ROACH, Units.HYRDRALISK, Units.INFESTOR, Units.ULTRALISK},
        Units.VIPER: set(),
    }

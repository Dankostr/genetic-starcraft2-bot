from sc2.units import Units

from starcraft.GameStateSnapshot import GameStateSnapshot
from starcraft.wrapper.ZergBuildings import ZergBuildings
from utils.singleton import Singleton


class GameState(object, metaclass=Singleton):

    def __init__(self):
        """
        Constructor for GameState object used to contain information about state of the game. Calling constructor
        fills object with starting information.
        Singleton metaclass is used to share instances between classes
        """
        self.overlords_in_building = 0
        self.minerals = 50
        self.vespene = 0
        self.drones_count = 12
        self.townhalls = [ZergBuildings.Buildings.HATCHERY]
        self.vespene_extractors = list()
        self.units = list()
        self.buildings = [ZergBuildings.Buildings.HATCHERY]
        self.army_limit = 14
        self.army_size = 12
        self.minerals_per_minute = 650
        self.vespene_per_minute = 0
        self.buildings_in_construction = []
        self.build_order = []
        self.ideal_worker_count=16
        self.missing_crystal_workers_count=4
        self.missing_vespene_workers_count=0
        self.timer  = 0
        self.queens_in_queue = 0
        self.under_attack = False
        self.upgrades = set()

    def update_state(self, minerals: int, vespene: int, drones_count: int, townhalls: Units,
                     vespene_extractors: Units,
                     units: Units,
                     buildings: Units, army_limit: int, army_size: int, minerals_per_minute: int,
                     vespene_per_minute: int,
                     buildings_in_construction : list,
                     path : list,
                     ideal_worker_count:int,
                     missing_crystal_workers_count:int,
                     missing_vespene_workers_count:int,
                     timer:float,
                     queens_in_queue,
                     under_attack:bool,
                     upgrades: set,
                     overlords_in_building:int):
        """
        Method to update state of the object

        :param minerals:
        :param vespene:
        :param drones_count:
        :param townhalls:
        :param vespene_extractors:
        :param units:
        :param buildings:
        :param army_limit:
        :param army_size:
        :param minerals_per_minute:
        :param vespene_per_minute:
        :param buildings_in_construction:
        :param path:
        :param ideal_worker_count:
        :param missing_crystal_workers_count:
        :param missing_vespene_workers_count:
        :param timer:
        :param queens_in_queue:
        :param under_attack:
        :param upgrades:
        :param overlords_in_building:
        """
        self.minerals = minerals
        self.vespene = vespene
        self.drones_count = drones_count
        self.townhalls = townhalls
        self.vespene_extractors = vespene_extractors
        self.units = units
        self.buildings = buildings
        self.army_limit = army_limit
        self.army_size = army_size
        self.minerals_per_minute = minerals_per_minute
        self.vespene_per_minute = vespene_per_minute
        self.buildings_in_construction = buildings_in_construction
        self.build_order = path
        self.ideal_worker_count = ideal_worker_count
        self.missing_crystal_workers_count = missing_crystal_workers_count
        self.missing_vespene_workers_count = missing_vespene_workers_count
        self.timer = timer
        self.queens_in_queue = queens_in_queue
        self.under_attack = under_attack
        self.upgrades = upgrades
        self.overlords_in_building = overlords_in_building

    def snapshot(self) -> GameStateSnapshot:
        """
        Creates GameStateSnapshot object with current information about game

        :return: GameStateSnapshot object
        """
        return GameStateSnapshot(
            self.minerals,
            self.vespene,
            self.drones_count,
            self.townhalls,
            self.vespene_extractors,
            self.units,
            self.buildings,
            self.army_limit,
            self.army_size,
            self.minerals_per_minute,
            self.vespene_per_minute,
            self.buildings_in_construction,
            self.build_order,
            self.ideal_worker_count,
            self.missing_crystal_workers_count,
            self.missing_vespene_workers_count,
            self.timer,
            self.queens_in_queue,
            self.under_attack,
            self.upgrades,
            self.overlords_in_building
        )

    def __repr__(self):
        return f"Minerals:{self.minerals}\n Vespene:{self.vespene}\n Drones Count:" \
               f"{self.drones_count} \n , Hatcheries: {self.townhalls} \n  Vespene Extractors:" \
               f"{self.vespene_extractors} \n Units : {self.units} \n Buildings: " \
               f"{self.buildings}\n Army limit:{self.army_limit} \n Army size:{self.army_size} \n" \
               f"Minerals per minute: {self.minerals_per_minute} \n Vespene per minute: {self.vespene_per_minute}"


class ZergValuesSnapshot(object):

    def __init__(self, fitness_values: dict):
        """
        Constructor for snapshot object of current fitness values

        :param fitness_values: Dictionary of ZergUnits.Units, ZergBuildings.Buildings  and ZergUpgrades.Upgrades enum and their
         values.
        """
        self.fitness_values = fitness_values.copy()

    def get_unit_fitness_value(self, item) -> float:
        """
        Returns fitness value for given enum from ZergUnits.Units, ZergBuildings.Buildings  and ZergUpgrades.Upgrades

        :param item: ZergUnits.Units, ZergBuildings.Buildings  and ZergUpgrades.Upgrades enum
        :return: Value of the item
        """
        return self.fitness_values[item]

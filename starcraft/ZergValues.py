from config import Config
from starcraft.ZergValuesSnapshot import ZergValuesSnapshot
from starcraft.wrapper.ZergBuildings import ZergBuildings
from starcraft.wrapper.SC2Helper import SC2Helper
from starcraft.wrapper.ZergUnits import ZergUnits
from starcraft.wrapper.ZergUpgrades import ZergUpgrades
from utils.singleton import Singleton


class ZergValues(object, metaclass=Singleton):

    def __init__(self):
        """
        Constructor for object that contains information about current fitness values of bot's buildings,
        army unit and upgrades.
        Singleton metaclass is used to share instances between classes.
        """
        self.fitness_values = ZergValues.__get_configured_values()
        self.sc2_helper = SC2Helper()

    def enemy_unit_spotted(self, unit_typeid: int):
        """
        Method to be called when enemy unit was spotted. It takes unit_typeid and updates value of units and buildings
        that are  countered by spotted unit as well as units and buildings that counter spotted unit

        :param unit_typeid: Type_id of an unit
        """
        enemy_unit = self.sc2_helper.get_army_unit_by_type_id(unit_typeid)
        if enemy_unit is None:
            return
        counters = self.sc2_helper.get_units_that_counter_given_unit(enemy_unit)
        are_countered = self.sc2_helper.get_units_that_are_countered_by_given_unit(enemy_unit)
        for counter in counters:
            self.increase_values(counter)
        for countered in are_countered:
            self.__decrease_values(countered)

    def enemy_unit_destroyed(self, unit_typeid):
        """
        Method to be called when enemy unit was destroyed. It takes unit_typeid and updates value of units and
        buildings that are countered by destroyed unit as well as units and buildings that counter destroyed unit

        :param unit_typeid: Type_id of an unit
        """
        enemy_unit = self.sc2_helper.get_army_unit_by_type_id(unit_typeid)
        if enemy_unit is None:
            return
        counters = self.sc2_helper.get_units_that_counter_given_unit(enemy_unit)
        are_countered = self.sc2_helper.get_units_that_are_countered_by_given_unit(enemy_unit)
        for counter in counters:
            self.__decrease_values(counter)
        for countered in are_countered:
            self.increase_values(countered)

    def __decrease_values(self, countered_unit):
        """
        Method to decrease value of dependencies for given unit and decrease value for given unit

        :param countered_unit: ZergUnits.Units enum
        """
        dependencies = self.sc2_helper.get_unit_dependencies(countered_unit)
        for dependency in dependencies:
            self.fitness_values[dependency] -= 3
        self.fitness_values[countered_unit] -= 2

    def increase_values(self, unit):
        """
        Method to increase value of dependencies for given unit and increase value for given unit

        :param unit: ZergUnits.Units enum
        """
        dependencies = self.sc2_helper.get_unit_dependencies(unit)
        for dependency in dependencies:
            self.fitness_values[dependency] += 3
        self.fitness_values[unit] += 2

    def snapshot(self) -> ZergValuesSnapshot:
        """
        Creates ZergValueSnapshot object of current fitness values.

        :return: ZergValueSnapshot object with current fitness values.
        """
        return ZergValuesSnapshot(self.fitness_values)

    @staticmethod
    def __get_configured_values():
        """
        Method to retrieve configured values from config object

        :return: Dictionary of ZergUnits.Units, ZergBuildings.Buildings  and ZergUpgrades.Upgrades enum and their
        configured values.
        """
        config = Config()
        values_units = {k: config[f"ZERG.UNITS.{k.name}"] for k in ZergUnits.units_list}
        values_buildings = {k: config[f"ZERG.BUILDINGS.{k.name}"] for k in
                            ZergBuildings.building_list}
        values_upgrades = {k: config[f"ZERG.UPGRADES.{k.name}"] for k in
                            ZergUpgrades.upgrades_list}
        values_units.update(values_buildings)
        values_units.update(values_upgrades)
        return values_units

    def __repr__(self):
        return str(self.fitness_values)

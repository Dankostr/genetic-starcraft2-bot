import threading
from queue import Queue

from loguru import logger
from sc2 import run_game, maps, Race
from sc2.player import Bot, Computer

from algorithm.genetic.AlgorithmScheduler import AlgorithmScheduler
from config import Config
from starcraft.GeneticBot import GeneticBot


def stop_algorithm():
    logger.info("Stopping algorithm")
    finish_event.set()
    stop_event.set()
    start_event.set()
    logger.info("Waiting for algorithm to stop")


if __name__ == "__main__":
    q = Queue(maxsize=10)
    config = Config()
    stop_event = threading.Event()
    start_event = threading.Event()
    finish_event = threading.Event()
    scheduler = AlgorithmScheduler(stop_event, q, start_event,finish_event)
    scheduler.start()
    try:
        if config["APP.GAME.START"]:
            run_game(maps.get(config["APP.GAME.MAP"]), [
                Bot(Race.Zerg, GeneticBot(stop_event, q, start_event)),
                Computer(config.get_race(), config.get_difficulty())
            ], realtime=True)
    finally:
        stop_algorithm()
